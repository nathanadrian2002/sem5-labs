#include<stdio.h>
#include<string.h>
#include<stdlib.h>


#include "preprocessor/preprocessor.h"
#include "parser/parser.h"

/*

Build with: 
	gcc -o main.o main.c preprocessor/preprocessor.c symbol/symbol.c \
token/token.c util/util.c parser/parser.c arguments/arguments.c

*/

void invalid()
{
    printf("----ERROR!---\n");
    exit(0);

}

int main()
{
	char SRC_FILE[] = "test.c", INTER_FILE[] = "test.temp"; 

	// Try to make these variables dynamically allocated
	// int position = 0, line = 1, column = 1;

	// Token t;
	// Symbol * symbol_table = NULL, s;

	Arguments args = args_init(SRC_FILE, INTER_FILE);
								// &line, &column, &position,
								// &t, &symbol_table, &s);

	// clear_symbol(&s);

	preprocess(args);

	get_next_token(args);

	// Temp Parser
	// Program --> 
	// if ((t.type == IDENTIFIER) && (strcmp(t.name, "main") == 0))
	// {
	// 	get_next_token(INTER_FILE, &line, &column, &position, &t);
	// 	update_symbol_table(t, &s, &symbol_table);
	// 	if ((t.type != SPEACIAL_SYMBOL) || (strcmp(t.name, "(") != 0))
	// 		invalid();
	// 	get_next_token(INTER_FILE, &line, &column, &position, &t);
	// 	update_symbol_table(t, &s, &symbol_table);
	// 	if ((t.type != SPEACIAL_SYMBOL) || (strcmp(t.name, ")") != 0))
	// 		invalid();
	// 	get_next_token(INTER_FILE, &line, &column, &position, &t);
	// 	update_symbol_table(t, &s, &symbol_table);
	// 	if ((t.type != SPEACIAL_SYMBOL) || (strcmp(t.name, "{") != 0))
	// 		invalid();
	// 	get_next_token(INTER_FILE, &line, &column, &position, &t);
	// 	update_symbol_table(t, &s, &symbol_table);
	// 	parser_gen_declarations(parser);
	// 	if ((t.type != SPEACIAL_SYMBOL) || (strcmp(t.name, "}") != 0))
	// 		invalid();
	// 	printf("Found main() {} \n");
	// }


	// Iterr
	while(strcmp(args.token_ptr->name,"") != 0)
	{
		print_token(*args.token_ptr);

		get_next_token(args);

	}

	print_symbol_table(args);

}