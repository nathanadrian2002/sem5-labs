#include "util.h"



int char_in_array(char c, char* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		if  (c == arr[i])
			return i+1;
	}

	return 0;
}

int string_in_array(char* str, char strs[100][100], int n)
{
	for (int i = 0; i < n; i++)
	{
		if (strcmp(str, strs[i]) == 0)
			return i+1;
	}
	return 0; 
}

int is_prepened(char * string, char * pre)
{
	int s = strlen(string);
	int p = strlen(pre);

	if (s < p)
		return 0;
	else
	{
		for (int i = 0; i < p; i++)
		{
			if (string[i] != pre[i])
				return 0;
		}
		return 1;
	}
}

int is_postpened(char* string, char * post)
{
	int s = strlen(string);
	int p = strlen(post);

	if (s < p)
		return 0;
	else
	{
		for (int i = 0; i < p; i++)
		{
			if (string[s-i-1] != post[p-i-1])
				return 0;
		}
		return 1;
	}
}