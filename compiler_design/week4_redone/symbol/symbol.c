
#include "symbol.h"

void print_symbol_table(Arguments args)
{
	Symbol* s = *args.symbol_table_ptr;
	if (s == NULL)
		printf("Symbol Table Is Empty\n");

	else
		printf("Serial\tName\tType\tData\tReturn\tArgs\n");

	while(s != NULL)
	{
		printf("%d\t%s\t%s\t", s->serial, s->lexeme_name, s->token_type);
		
		if (strcmp(s->token_type, "func") == 0)
			printf("%s\t%s\t%d", "---", s->return_type, s->num_args);
		else if (strcmp(s->token_type, "var") == 0)
			printf("%s\t%s \t%s", s->data_type,"---", "--");
		
		printf("\n");

		s = s->next;
	}
}

int table_length(Symbol* s)
{
	int result = 0;

	while(s != NULL)
	{
		result++;
		s = s->next;
	}
	return result;

}


void copy_symbol(Symbol* dest, Symbol src)
{
	dest->serial = src.serial;
	strcpy(dest->lexeme_name, src.lexeme_name);
	strcpy(dest->token_type, src.token_type);
	strcpy(dest->data_type, src.data_type);
	strcpy(dest->return_type, src.return_type);
	dest->num_args = src.num_args;
}

void clear_symbol(Symbol* s)
{
	s->serial = 0;
	strcpy(s->lexeme_name, "");
	strcpy(s->token_type, "");
	strcpy(s->data_type, "");
	strcpy(s->return_type, "");
	s->num_args = 0;
}

void add_symbol(Symbol** table, Symbol sample)
{
	Symbol* entry = (Symbol*)calloc(1, sizeof(Symbol));
	Symbol *temp = *table; 
	int count = 0;
	copy_symbol(entry, sample);
	
	entry->serial = count;

	if (*table == NULL)
		*table = entry;
	else
	{
		count = 1;
		while (temp->next != NULL)
		{	
			if (strcmp(entry->lexeme_name, temp->lexeme_name) == 0)
				return;
			temp = temp->next;
			count++;
		}
		// printf("Passing by: %s\n", temp->lexeme_name);
		if (strcmp(entry->lexeme_name, temp->lexeme_name) == 0)
			return;
		entry->serial = count;
		temp->next = entry;
	}

}

void update_symbol_table(Arguments args)
// void update_symbol_table(Token token, Symbol* cache, Symbol** table)
{
	Token token = *args.token_ptr;
	Symbol *cache = args.cache_symbol;
	Symbol **table = args.symbol_table_ptr;
	
	if (token.type == KEYWORD)
	{
		
		if (strcmp(cache->token_type, "func") == 0)
		{
			cache->num_args++;
			return;
		}
		
		// printf("Found keyword\n");
		strcpy(cache->data_type, token.name);
		strcpy(cache->return_type, token.name);
		return;
	}

	if (token.type == IDENTIFIER)
	{
		// printf("Found identifier\n");
		if (strcmp(cache->token_type, "func") == 0)
			return;
		strcpy(cache->lexeme_name, token.name);
		return;
	}

	if (strcmp(token.name, "(") == 0)
	{	
		strcpy(cache->token_type, "func");
		return;
	}

	if (strcmp(token.name, ",") == 0)
	{
		if (strcmp(cache->token_type, "func") == 0)
			return;
		strcpy(cache->token_type, "var");
		add_symbol(table, *cache);
		return;
	}

	if (strcmp(token.name, ";") == 0)
	{
		strcpy(cache->token_type, "var");
		add_symbol(table, *cache);
		clear_symbol(cache);
		return;
	}

	if (strcmp(token.name, ")") == 0)
	{
		if (strcmp(cache->token_type, "func") != 0)
			return;

		add_symbol(table, *cache);
		clear_symbol(cache);
		return;
	}


}
