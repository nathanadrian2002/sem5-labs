#pragma once

#include<string.h>
#include<stdlib.h>

#include "../token/token.h"
#include "../arguments/arguments.h"


// void print_symbols(Symbol* s);
void print_symbol_table(Arguments args);
int table_length(Symbol* s);

void copy_symbol(Symbol* dest, Symbol src);
void clear_symbol(Symbol* s);

void add_symbol(Symbol** table, Symbol sample);

void update_symbol_table(Arguments args);
// void update_symbol_table(Token token, Symbol* cache, Symbol** table);
