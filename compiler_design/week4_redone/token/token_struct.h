#pragma once

enum Type{
	
	ARITHMETIC,
	RELATIONAL,
	LOGICAL,
	SPEACIAL_SYMBOL,
	KEYWORD,
	IDENTIFIER,

};

typedef struct Token{
	int row;
	int column;
	char name[20];
	enum Type type;

} Token;