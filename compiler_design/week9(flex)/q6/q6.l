
%{
	int s = 0;
%}

%%

[A-Z] {if(s == 0) {fprintf(yyout, "%c", yytext[0] ^ ' ');}
		else fprintf(yyout, yytext);
}
"//" { s = 1; fprintf(yyout, yytext);}
"/*" { s = 2; fprintf(yyout, yytext);}
\n {s = (s == 1) ? 0 : s; fprintf(yyout, yytext);}
"*/" {s = (s == 2) ? 0 : s; fprintf(yyout, yytext);}

%%



int main(int argc, char **argv)
{
	yyin = fopen("input.c", "r");
	yyout = fopen("output.c", "w");
	yylex();
	printf("\n");
	printf("File Generated\n");

}
	
int yywrap()
{
	return 1;
}
