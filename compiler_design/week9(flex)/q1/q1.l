%{
int vowels = 0;
int consonants = 0;
%}

%%
[a|e|i|o|u|A|E|I|O|U] { vowels++; }
[a-zA-Z] { consonants++;}

%%

int main()
{
	yylex();
	printf("\nVowels: %d Consonants: %d\n", vowels, consonants); 
}
	
int yywrap()
{
	return 1;
}

