%{
int pints = 0;
int nints = 0;
int pfloats = 0;
int nfloats = 0;
%}

%%
-[0-9]+\.[0-9]* { nfloats++; }
\+?[0-9]+\.[0-9]* { pfloats++; }
-[0-9]+ { nints++; }
\+?[0-9]+ { pints++; }
%%

int main(int argc, char **argv)
{
	yylex();
	printf("\n");
	printf("Positive Floats: %d\n", pfloats);
	printf("Negative Floats: %d\n", nfloats); 
	printf("Positive Ints: %d\n", pints); 
	printf("Negative Ints: %d\n", nints); 


}
	
int yywrap()
{
	return 1;
}
