%{
int chars = 0;
int words = 0;
int lines = 0;
int blanks = 0;
%}

%%
[a-zA-Z]+ { words++; chars += strlen(yytext); }
\n { chars++; lines++; }
" " {blanks++;}
. { chars++; }
%%

int main(int argc, char **argv)
{
	yylex();
	printf("\n");
	printf("Lines: %d\n", lines);
	printf("Words: %d\n", words); 
	printf("Chars: %d\n", chars); 
	printf("Blanks: %d\n", blanks); 


}
	
int yywrap()
{
	return 1;
}

