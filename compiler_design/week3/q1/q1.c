#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Token{
	int row;
	int column;
	char type[20];
} Token;

void print_token(Token t){
	// printf("Token: %s\n", t.type);
    printf("< %s , r : %d , c : %d >\n", t.type, t.row, t.column);
}

int char_in_array(char c, char* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		if  (c == arr[i])
			return i+1;
	}

	return 0;
}

int string_in_array(char* str, char strs[100][100], int n)
{
	for (int i = 0; i < n; i++)
	{
		if (strcmp(str, strs[i]) == 0)
			return i+1;
	}
	return 0; 
}

int is_arithmetic(FILE * file_ptr, int reset_to, Token* t)
{
	char c = fgetc(file_ptr);
	char arith_ops[] = {'+', '-', '*', '/'};
	int num_ops = 4, i;
	// printf("Arith : %c", c);
	if (i = char_in_array(c, arith_ops, num_ops))
	{
		strcpy(t->type, (char[2]){arith_ops[i - 1], '\0'});
		return 1;
	}

	return 0;
}

int is_relational(FILE* file_ptr, int reset_to, Token* t)
{
	int result = 0, i;
	char c = fgetc(file_ptr);
	// char c[3];
	char ops[] = {'!', '>', '<'};

	if (c == '=')
	{
		c = fgetc(file_ptr);
		if (c == '=')
		{
			strcpy(t->type, "==");
		}
		else{
			strcpy(t->type, "=");
		}	
		return 1;
	}
	else if (i = char_in_array(c, ops, 3))
	{
		c = fgetc(file_ptr);
		if (c == '=')
		{	
			strcpy(t->type, (char[3]) {ops[i-1], '=', '\0'});
			return 1;
		}
	}

	fseek(file_ptr, reset_to, SEEK_SET);
	return 0;
}

int is_logical(FILE* file_ptr, int reset_to, Token* t)
{
	char c = fgetc(file_ptr);

	if (c == '!')
	{
		strcpy(t->type, "!");
		return 1;
	}
	else if (c == '|')
	{
		c = fgetc(file_ptr);
		if (c == '|')
		{	
			strcpy(t->type, "||");
			return 1;
		}
	}
	else if (c == '&')
	{
		c = fgetc(file_ptr);
		if (c == '&')
		{
			strcpy(t->type, "&&");
			return 1;
		}
	}


	fseek(file_ptr, reset_to, SEEK_SET);
	return 0;
}

int is_special_symbol(FILE* file_ptr, int reset_to, Token* t)
{
	char ops[] = {'(', ')','{', '}', ',', ';'};
	int num_ops = 6;

	char c = fgetc(file_ptr);
	int result = char_in_array(c, ops, num_ops);

	strcpy(t->type, (char[2]){ops[result-1], '\0'});
	
	fseek(file_ptr, reset_to, SEEK_SET);
	return result;

}

int is_keyword(FILE* file_ptr, int reset_to, Token* t)
{
	char* tokens[50] = {"auto", "for", "if", "goto",
						 "char", "int", "float", "return",
						 "double", "struct", "else", "long",
						 "case", "break", "continue", "switch",
						 "main", "void"
						};
	int num = 18;

	char buff[2000], c;
	int i = 0;

	c = fgetc(file_ptr);

	while (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'))
	{
		buff[i++] = c;
		c = fgetc(file_ptr);
	}
	buff[i]= '\0';	

	for (int i = 0; i < num; i++)
	{
		// printf("Comparing %s\n", tokens[i]);
		if (strcmp(tokens[i], buff) == 0)
		{	
			strcpy(t->type, buff);
			return 1;
		}
	}

	fseek(file_ptr, reset_to, SEEK_SET);
	return 0;
}

int is_numeric_constant(FILE* file_ptr, int reset_to, Token* t)
{
	char c = fgetc(file_ptr);
	char buff[100]; int b = 0;
	int result = 0;
	// int decimal = 0;
	
	while (('0' <= c && c <= '9') || c == '.')
	{
		buff[b++] = c;
		c = fgetc(file_ptr);
		result = 1;
	}
	buff[b] = '\0';

	if (result)
	{
		strcpy(t->type, buff);
	}	
	fseek(file_ptr, reset_to, SEEK_SET);
	return result;
}

int is_string_literal(FILE* file_ptr, int reset_to, Token* t)
{
	char c = fgetc(file_ptr);
	char buff[100], b = 0;

	if (c == '"')
	{
		do{
			buff[b++] = c;
			c = fgetc(file_ptr);
		} while(c != '"');
		buff[b++] = c;
		buff[b] = '\0';
		strcpy(t->type, buff);
		return 1;
		
	}
	else{
		fseek(file_ptr, reset_to, SEEK_SET);
		return 0;
	}

}

int is_identifier(FILE* file_ptr, int reset_to, Token* t)
{
	char c = fgetc(file_ptr);
	char buff[100], b = 0;

	if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('_' == c))
	{
		// printf("Found1 %c", c);
		buff[b++] = c;
		c = fgetc(file_ptr);
		while( ('a' <= c && c <= 'z') 
			|| ('A' <= c && c <= 'Z') 
			|| ('0' <= c && c <= '9') 
			|| ('_' == c ))
		{
			buff[b++] = c;
			c = fgetc(file_ptr);
			// printf("Loop: %c", c);
		}
		buff[b] = '\0';
		strcpy(t->type, buff);
		return 1;
	}
	else
		return 0;
}

void get_next_token(char* file_name, int* l, int* col, int* pos,Token* t)
{
	FILE *fp =fopen(file_name, "rb+");
	char c = 0;
	// int position = *pos, line = *l, column = *col;
	int is_token;

	if (fp == NULL)
	{
		printf("Cannot open file \n");
		exit(0);
	}
	
	fseek(fp, *pos, SEEK_SET);

	while(c!=EOF)
	{
		is_token = 1;
	
		if (is_arithmetic(fp, *pos, t));
		else if (is_relational(fp, *pos, t));
		else if (is_logical(fp, *pos, t));
		else if (is_special_symbol(fp, *pos, t));
		else if (is_keyword(fp, *pos, t));
		else if (is_numeric_constant(fp, *pos, t));
		else if (is_string_literal(fp, *pos, t));
		else if (is_identifier(fp, *pos, t));
		else{
		
			fseek(fp, *pos, SEEK_SET);
			
			(*pos)++;
			(*col)++;
			c = fgetc(fp);

			if (c == '\n')
			{
				(*l)++; (*col) = 1;
			}

			is_token = 0;
		}
		
		if (is_token)
		{
			t->row = *l;
			t->column = *col;
			fseek(fp, *pos, SEEK_SET);

			*pos += strlen(t->type);
			*col += strlen(t->type);
			
			for (int i = 0; i < strlen(t->type); i++)
			{
				c = fgetc(fp);
	
				if (c == '\n')
				{
					*l++; 
					*col = 1;
				}
			}
			return;			
		}	

	}

	strcpy(t->type, "");
	return;

}


int main()
{
	// char buf[10];
	// FILE *fp=fopen("t.txt","rb+");
	
	int position = 0, line = 1, column = 1, is_token;
	char c = 0;

	Token t;
	
	get_next_token("t.txt", &line, &column, &position, &t);
	// get_next_token("t.txt", &line, &column, &position, &t);
	
	while(strcmp(t.type,"") != 0)
	{
		print_token(t);
		get_next_token("t.txt", &line, &column, &position, &t);

	}

}