#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int ind = 0;
char str[256];


void invalid()
{
    printf("----ERROR!---\n");
    exit(0);

}


void S();
void A();
void Aprime();
void B();


void S()
{
    if (str[ind] != 'a')
        invalid();
    
    ind++;
    A();

    if (str[ind] != 'c')
        invalid();
    
    ind++;
    B();

    if (str[ind] != 'e')
        invalid();

    ind++;
    
}

void A()
{
    if (str[ind] == 'b')
    {
        ind++;
        Aprime();
    }
    else
        invalid();
}

void Aprime()
{
    if (str[ind] == 'b')
    {
        ind++;
        Aprime();
    }
}

void B()
{
    if (str[ind] == 'd')
        ind++;
    else
        invalid();  
}

void main()
{
    printf("ENTER STRING : ");
    scanf("%s", str);

    str[strlen(str)] = '$';

    S();

    if (str[ind] == '$')
        printf("Valid\n");
    else
        invalid();
}