#pragma once

#include <string.h>
#include <stdlib.h>

#include "../token/token_struct.h"
#include "../symbol/symbol_struct.h"

typedef struct Arguments{

	char source_file_name[50], intermediate_file_name[50];

	int *line_ptr, *column_ptr, *position_ptr;

	Token *token_ptr;

	Symbol **symbol_table_ptr, *cache_symbol;

} Arguments;

Arguments args_init(char s_fname[], char t_fname[]);
					// int *lptr, 
					// int *cptr,
					// int *pptr, 
					// Token *tptr, 
					// Symbol **stp, 
					// Symbol *cs);
