struct Symbol{
	
	int serial, 
		num_args, scope;

	char lexeme_name[100], 
		 data_type[10],
		 return_type[10],
		 token_type[10];

	struct Symbol * next;

};

typedef struct Symbol Symbol;