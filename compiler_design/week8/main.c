#include<stdio.h>
#include<string.h>
#include<stdlib.h>


#include "preprocessor/preprocessor.h"
#include "token/token.h"
#include "parser/parser.h"

/*

Build with: 
	gcc -o main.o main.c preprocessor/preprocessor.c symbol/symbol.c \
token/token.c util/util.c parser/parser.c arguments/arguments.c

*/

// TODO
// Fix array types for symbol table

int main()
{
	char SRC_FILE[] = "test.c", INTER_FILE[] = "test.temp"; 


	Arguments args = args_init(SRC_FILE, INTER_FILE);


	preprocess(args);

	// Parsing
	if(parser_program(args))
		printf("Valid Parsing\n");
	
	// print_symbol_table(args);

}