#pragma once

#include <stdio.h>

#include "../arguments/arguments.h"
#include "../token/token.h"

void parser_invalid_grammer(char message[]);
void parser_check_token(Arguments args, char* name, enum Type type);

int parser_program(Arguments args);

void parser_declarations(Arguments args);
int parser_datatype(Arguments args);
void parser_identifier_list_1(Arguments args);
void parser_identifier_list_2(Arguments args);
void parser_arr_list(Arguments args);

void parser_statement_list(Arguments args);
int parser_statment(Arguments args);

int parser_assignment(Arguments args);

int parser_decision(Arguments args);
void parser_decision_else(Arguments args);
void parser_decision_prime(Arguments args);

int parser_looping(Arguments args);

void parser_expression(Arguments a);

int parser_relop(Arguments a);
int parser_mulop(Arguments a);
int parser_addop(Arguments a);

void parser_tprime(Arguments a);
void parser_factor(Arguments a);
void parser_term(Arguments a);
void parser_eprime(Arguments a);
void parser_simple_expression(Arguments a);
void parser_seprime(Arguments a);
// void parser_arr_list(Arguments args);

void parser_assignments_1(Arguments args);
void parser_assignments_2(Arguments args);
