#include "parser.h"


/*
	Derivations:
		program --> main() { declarations statement-list }
		
		declarations --> datatype id-list1 ; declarations | lambda
		datatype --> int | char
		id-list1 --> id id-list2 
		id-list2 --> id-arr-list, id-list1 | lambda
		id-arr-list --> [ NUMERIC_CONSTANT ] | lambda
		
		statement-list --> statment statement-list | lambda

		statement --> assignment ; | decision | looping
		
		assignment --> id = expn
		
		expn --> simple-expn eprime
		simple-expn --> term seprime
		term --> factor tprime

		tprime --> mulop factor tprime | lambda
		seprime --> addop term seprime | lambda
		eprime --> relop simple-expn | lambda
		
		factor --> id | num
		mulop --> * | / | %
		addop --> + | -
		relop --> == | != | <= | >= | < | >


		decision -->  if ( expn ) { statment-list } decision-else
		decision-else --> else decision-prime | lambda
		decision-prime --> decision | { statment-list }

		looping --> while ( expn ) { statment-list } |
					for ( assignment ; expn ; assignment ) { statment-list } 


	Returns 
		0 : Invalid
		1 : Valid
*/

void parser_check_token(Arguments args, char* name, enum Type type)
{
	Token* t = args.token_ptr;
	char buffer[50];
	sprintf(buffer, "Missing %s", name);
	
	if (!token_compare(*t, name, type))
		parser_invalid_grammer(buffer);
	get_next_token(args);
}

int parser_program(Arguments args)
{
	Token* t = args.token_ptr;
	get_next_token(args);
	
	// if (!token_compare(*t, "main", IDENTIFIER))
	// 	parser_invalid_grammer("Missing Main Method");
	
	// get_next_token(args);

	parser_check_token(args, "main", IDENTIFIER);

	parser_check_token(args, "(", SPECIAL_SYMBOL);

	parser_check_token(args, ")", SPECIAL_SYMBOL);

	parser_check_token(args, "{", SPECIAL_SYMBOL);
	
	parser_declarations(args);

	parser_statement_list(args);
	
	parser_check_token(args, "}", SPECIAL_SYMBOL);	

	return 1;
}


void parser_statement_list(Arguments args)
{

	if (parser_statment(args))
		parser_statement_list(args);
}

int parser_statment(Arguments args)
{

	if (parser_assignment(args))
	{
		parser_check_token(args, ";", SPECIAL_SYMBOL);
		return 1;
	}
	if (parser_decision(args))
		return 1;
	if (parser_looping(args))
		return 1;

	return 0;

}

int parser_looping(Arguments args)
{
	Token* t = args.token_ptr;
	if (token_compare(*t, "while", KEYWORD))
	{
		get_next_token(args);
		
		parser_check_token(args, "(", SPECIAL_SYMBOL);

		parser_expression(args);

		parser_check_token(args, ")", SPECIAL_SYMBOL);

		parser_check_token(args, "{", SPECIAL_SYMBOL);


		parser_statement_list(args);


		parser_check_token(args, "}", SPECIAL_SYMBOL);

		return 1;

	}
	
	if (token_compare(*t, "for", KEYWORD))
	{
		
		// printf("Entered FOr loop\n");
		get_next_token(args);

		parser_check_token(args, "(", SPECIAL_SYMBOL);

		parser_assignment(args);

		parser_check_token(args, ";", SPECIAL_SYMBOL);

		parser_expression(args);

		parser_check_token(args, ";", SPECIAL_SYMBOL);
		

		parser_assignment(args);

		// printf("FOund assignment\n");

		parser_check_token(args, ")", SPECIAL_SYMBOL);


		parser_check_token(args, "{", SPECIAL_SYMBOL);


		parser_statement_list(args);


		parser_check_token(args, "}", SPECIAL_SYMBOL);


		return 1;
	}
	
	return 0;
}

int parser_decision(Arguments args)
{
	Token* t = args.token_ptr;
	if (token_compare(*t, "if", KEYWORD))
	{
		get_next_token(args);
		parser_check_token(args, "(", SPECIAL_SYMBOL);

		parser_expression(args);

		parser_check_token(args, ")", SPECIAL_SYMBOL);

		parser_check_token(args, "{", SPECIAL_SYMBOL);


		parser_statement_list(args);


		parser_check_token(args, "}", SPECIAL_SYMBOL);

		parser_decision_else(args);

		return 1;
	}

	return 0;
}

void parser_decision_else(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, "else", KEYWORD))
	{
		get_next_token(args);

		parser_decision_prime(args);
	}
}


void parser_decision_prime(Arguments args)
{
	
	Token* t = args.token_ptr;

	if (parser_decision(args));
	else if (token_compare(*t, "{", SPECIAL_SYMBOL))
	{
		get_next_token(args);

		parser_statement_list(args);

		parser_check_token(args, "}", SPECIAL_SYMBOL);

	}
	else
		parser_invalid_grammer("Incorrect If Else");

}

int parser_assignment(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, NULL, IDENTIFIER))
	{
		// printf("Found On1 ID\n");
		get_next_token(args);

		if (!token_compare(*t, "=", SPECIAL_SYMBOL))
			parser_invalid_grammer("Missing =");

		get_next_token(args);

		// printf("Entering Expresssion\n");
		parser_expression(args);

		return 1;
	}
	
	return 0;
}

void parser_expression(Arguments a)
{
	parser_simple_expression(a);
	
	parser_eprime(a);

}

void parser_simple_expression(Arguments a)
{
	parser_term(a);

	parser_seprime(a);
}

void parser_eprime(Arguments a)
{
	if (parser_relop(a))
	{
		parser_simple_expression(a);
	}
	// else 
	// 	printf("Eprime Null\n");
}

void parser_seprime(Arguments a)
{
	if (parser_addop(a))
	{
		parser_term(a);

		parser_seprime(a);
	}
	// else 
	// 	printf("SePrim NUll\n");
}

int parser_addop(Arguments a)
{
	Token* t = a.token_ptr;

	if (token_compare(*t, "+", ARITHMETIC)
	||  token_compare(*t, "-", ARITHMETIC))
	{
		get_next_token(a);
		return 1;
	}	
	return 0;
}

void parser_term(Arguments a)
{
	parser_factor(a);

	// printf("Got factor\n");

	parser_tprime(a);
}


void parser_factor(Arguments a)
{
	Token* t = a.token_ptr;

	if (token_compare(*t, NULL, IDENTIFIER)
	||  token_compare(*t, NULL, NUMERIC_CONSTANT))
	{
		get_next_token(a);
		return;
	}

	parser_invalid_grammer("Missing ID or Number");

}

void parser_tprime(Arguments a)
{
	if (parser_mulop(a))
	{
		parser_factor(a);

		parser_tprime(a);
	}
	// else	
	// 	printf("Tpr NULl\n");
}

int parser_mulop(Arguments a)
{
	Token* t = a.token_ptr;

	if (token_compare(*t, "*", ARITHMETIC)
	||  token_compare(*t, "/", ARITHMETIC)
	||  token_compare(*t, "%", ARITHMETIC))
	{
		get_next_token(a);
		return 1;
	}	
	return 0;
}

int parser_relop(Arguments a)
{
	Token* t = a.token_ptr;

	if (token_compare(*t, NULL, RELATIONAL))
	{	
		get_next_token(a);
		return 1;
	}
	// else
		// printf("")
		// get_next_token(a);
		
	return 0;

}


void parser_declarations(Arguments args)
{
	Token* t = args.token_ptr;
	
	// if (token_compare(*t, "int", KEYWORD))
	if (parser_datatype(args))
	{
		get_next_token(args);
		
		parser_identifier_list_1(args);

		if (!token_compare(*t, ";", SPECIAL_SYMBOL))
			parser_invalid_grammer("Missing ;");	

		get_next_token(args);

		parser_declarations(args);

	}

}

void parser_identifier_list_1(Arguments args)
{
	Token* t = args.token_ptr;
	// printf("Calling ID List 1\n");
	
	if (!token_compare(*t, NULL, IDENTIFIER))
		parser_invalid_grammer("Missing Identifier");

	get_next_token(args);

	parser_identifier_list_2(args);



}


void parser_identifier_list_2(Arguments args)
{
	Token* t = args.token_ptr;

	parser_arr_list(args);

	if (token_compare(*t, ",", SPECIAL_SYMBOL))
	{
		// printf("Found Coomma\n");
		get_next_token(args);
		parser_identifier_list_1(args);
	}

}


void parser_arr_list(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, "[", SPECIAL_SYMBOL))
	{
		// printf("Found Coomma\n");
		get_next_token(args);
		
		if (!token_compare(*t, NULL, NUMERIC_CONSTANT))
			parser_invalid_grammer("Missing Numeric Constant");

		get_next_token(args);

		if (!token_compare(*t, "]", SPECIAL_SYMBOL))
			parser_invalid_grammer("Missing ]");

		get_next_token(args);

	}
}


int parser_datatype(Arguments args)
{
	Token* t = args.token_ptr;
	if (token_compare(*t, "int", KEYWORD) 
	||  token_compare(*t, "char", KEYWORD))
		return 1;
	return 0;
}


void parser_invalid_grammer(char message[])
{	
    printf("ERROR: %s\n", message);
    exit(0);
}