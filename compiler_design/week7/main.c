#include<stdio.h>
#include<string.h>
#include<stdlib.h>


#include "preprocessor/preprocessor.h"
#include "token/token.h"
#include "parser/parser.h"

/*

Build with: 
	gcc -o main.o main.c preprocessor/preprocessor.c symbol/symbol.c \
token/token.c util/util.c parser/parser.c arguments/arguments.c

*/


int main()
{
	char SRC_FILE[] = "test.c", INTER_FILE[] = "test.temp"; 

	// Try to make these variables dynamically allocated
	// int position = 0, line = 1, column = 1;

	// Token t;
	// Symbol * symbol_table = NULL, s;

	Arguments args = args_init(SRC_FILE, INTER_FILE);
								// &line, &column, &position,
								// &t, &symbol_table, &s);

	// clear_symbol(&s);

	preprocess(args);

	// Parsing
	if(parser_program(args))
		printf("Valid Parsing\n");
	

	print_symbol_table(args);

}