#include "parser.h"


/*
	Derivations:
		program --> main() { declarations assignments1 }
		
		declarations --> datatype id-list1 ; declarations | lambda
		datatype --> int | char
		id-list1 --> id id-list2
		id-list2 --> , id-list1 | lambda

		assignments1 --> id = assignments2 ; assignments1 | lambda
		assignmnets2 -> id | num

	Returns 
		0 : Invalid
		1 : Valid
*/

int parser_program(Arguments args)
{
	Token* t = args.token_ptr;
	get_next_token(args);
	if (!token_compare(*t, "main", IDENTIFIER))
		parser_invalid_grammer("Missing Main Method");
	
	get_next_token(args);
	if (!token_compare(*t, "(", SPECIAL_SYMBOL))
		parser_invalid_grammer("Missing (");
	
	get_next_token(args);
	if (!token_compare(*t, ")", SPECIAL_SYMBOL))
		parser_invalid_grammer("Missing )");
	
	get_next_token(args);
	if (!token_compare(*t, "{", SPECIAL_SYMBOL))
		parser_invalid_grammer("Missing {");


	get_next_token(args);
	
	parser_declarations(args);


	parser_assignments_1(args);

	
	if (!token_compare(*t, "}", SPECIAL_SYMBOL))
		parser_invalid_grammer("Missing }");	
		// print_token(*t);
	
	return 1;
}

void parser_assignments_1(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, NULL, IDENTIFIER))
	{
		get_next_token(args);

		if (!token_compare(*t, "=", RELATIONAL))
			parser_invalid_grammer("Missing =");

		get_next_token(args);

		parser_assignments_2(args);

		if (!token_compare(*t, ";", SPECIAL_SYMBOL))
			parser_invalid_grammer("Missing ;");
		get_next_token(args);

		parser_assignments_1(args);


	}

}

void parser_assignments_2(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, NULL, IDENTIFIER)
	||	token_compare(*t, NULL, NUMERIC_CONSTANT))
		get_next_token(args);
	
	else
		parser_invalid_grammer("Missing Identifier or Constant");

}


void parser_declarations(Arguments args)
{
	Token* t = args.token_ptr;
	
	// if (token_compare(*t, "int", KEYWORD))
	if (parser_datatype(args))
	{
		get_next_token(args);
		
		parser_identifier_list_1(args);

		if (!token_compare(*t, ";", SPECIAL_SYMBOL))
			parser_invalid_grammer("Missing ;");	

		get_next_token(args);

		parser_declarations(args);

	}

}

void parser_identifier_list_1(Arguments args)
{
	Token* t = args.token_ptr;

	if (!token_compare(*t, NULL, IDENTIFIER))
		parser_invalid_grammer("Missing Identifier");

	get_next_token(args);

	parser_identifier_list_2(args);

	// if (!token_compare(*t, ";", SPECIAL_SYMBOL))
	// 	parser_invalid_grammer("Missing ;");	
	
	// get_next_token(args);
	// parser_declarations(args);
}

void parser_identifier_list_2(Arguments args)
{
	Token* t = args.token_ptr;

	if (token_compare(*t, ",", SPECIAL_SYMBOL))
	{
		// printf("Found Coomma\n");
		get_next_token(args);
		parser_identifier_list_1(args);
	}

}

int parser_datatype(Arguments args)
{
	Token* t = args.token_ptr;
	if (token_compare(*t, "int", KEYWORD) 
	||  token_compare(*t, "char", KEYWORD))
		return 1;
	return 0;
}


void parser_invalid_grammer(char message[])
{	
    printf("ERROR: %s\n", message);
    exit(0);
}