#pragma once

#include <stdio.h>

#include "../arguments/arguments.h"
#include "../token/token.h"

void parser_invalid_grammer(char message[]);

int parser_program(Arguments args);

void parser_declarations(Arguments args);
int parser_datatype(Arguments args);
void parser_identifier_list_1(Arguments args);
void parser_identifier_list_2(Arguments args);

void parser_assignments_1(Arguments args);
void parser_assignments_2(Arguments args);
