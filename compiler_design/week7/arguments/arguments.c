#include "arguments.h"


Arguments args_init(char s_fname[], char t_fname[])
					// int *lptr, int *cptr, int *pptr, 
					// Token *tptr, Symbol **stp, Symbol *cs)
{
	Arguments a;

	strcpy(a.source_file_name, s_fname); 
	strcpy(a.intermediate_file_name, t_fname);

	a.line_ptr = (int*)calloc(1, sizeof(int)); 
	*a.line_ptr = 1;

	a.column_ptr = (int*)calloc(1, sizeof(int)); 
	a.position_ptr = (int*)calloc(1, sizeof(int)); 

	a.token_ptr = (Token*)calloc(1, sizeof(Token));

	a.symbol_table_ptr = (Symbol**)calloc(1, sizeof(Symbol*));

	a.cache_symbol = (Symbol*)calloc(1, sizeof(Symbol));

	return  a;

}