#pragma once

enum Type{
	
	ARITHMETIC,
	RELATIONAL,
	LOGICAL,
	SPECIAL_SYMBOL,
	KEYWORD,
	IDENTIFIER,
	NUMERIC_CONSTANT

};

typedef struct Token{
	
	char name[20];
	enum Type type;
	int row, column;

} Token;