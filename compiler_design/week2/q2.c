#include<stdio.h>
#include <stdio.h>
#include <string.h>

void substring(char string[], int l, int r, char out[])
{
	int i = 0;

	while(l < r)
	{
		out[i++] = string[l++];
	}
}


int isPD(char *line) {
	
	char pd[][100] = {"include", "define "};
	char sub[100];
	if (*line != '#')   
		return 0;

	for (int i =0; i < 2; i++)
	{
		
		if (strlen(line) < strlen(pd[i]) + 1)
			return 0;

		substring(line, 1, 1+ strlen(pd[i]), sub);
		// printf("")
		if (strcmp(pd[i], sub) == 0)
		{	
			return 1;
		}
	}

	return 0;

}

int main() {

    FILE *in = fopen("temp.c", "r"), 
    	*out = fopen("r1.txt", "w+");
    if (!in) {
        perror("Error opening input file");
        return 1;
    }

    char line[1000];
    while (fgets(line, sizeof(line), in)) {


    	
    	if (isPD(line))
    		continue;
    	// printf("%s", line);
    	fputs(line, out);

    }

    fclose(in);
    fclose(out);

    return 0;
}