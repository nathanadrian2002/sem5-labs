%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
int yyerror();
%}

%token NUM
%token NEWLINE
%token ADD SUB MUL DIV POW NEG

%%

input:
      /* empty */
    | input line
    ;

line:
      NEWLINE
    | exp NEWLINE   { printf("Valid postfix expression.\n"); }
    ;

exp:
      NUM
    | exp exp ADD
    | exp exp SUB
    | exp exp MUL
    | exp exp DIV
    | exp exp POW
    | exp NEG
    ;

%%


int yyerror(char *msg)
{
	printf("Invalid Expression\n");
	exit(0);
}
void main ()
{
	printf("Enter the expression\n");
	yyparse();
}

