


%{
#include "q4.tab.h"
%}

%%

[ \t]+          { /* Skip whitespace */ }
[0-9]+          { return NUM; }
"\n"            { return NEWLINE; }
"+"             { return ADD; }
"-"             { return SUB; }
"*"             { return MUL; }
"/"             { return DIV; }
"^"             { return POW; }
"n"             { return NEG; }
.               { fprintf(stderr, "Unrecognized character: %s\n", yytext); return 0; }

%%

int yywrap() {
	return 1;
}
