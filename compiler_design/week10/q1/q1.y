%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
int yyerror();
%}

%token INT CHAR ID NUMERIC_CONSTANT
%left ',' ';'

%%

declaration:
	datatype id_list1 ';' {
		printf("Valid declaration\n");
		exit(0);
	}
;

datatype:
	INT		
	| CHAR
;

id_list1:
	ID id_list2 
;

id_list2:
	/* Lambda */
	| id_arr_list ',' id_list1
;

id_arr_list:
	/* Lambda */
	| '[' NUMERIC_CONSTANT ']' 
;

%%

int yyerror(char *msg)
{
	printf("Invalid Expression\n");
	exit(0);
}
void main ()
{
	printf("Enter the expression\n");
	yyparse();
}
