
%{
#include "q1.tab.h" /* include Bison-generated token definitions */
%}

digit       [0-9]
letter      [a-zA-Z]
identifier  {letter}({letter}|{digit})*
numeric     {digit}+

%%

"int"               { return INT; }
"char"              { return CHAR; }
{identifier}        { return ID; }
{numeric}           { yylval = atoi(yytext); return NUMERIC_CONSTANT; }
";"                 { return ';'; }
","                 { return ','; }
"["                 { return '['; }
"]"                 { return ']'; }
[ \t\n]+            { /* ignore whitespace */ }
.                   { fprintf(stderr, "Unrecognized character: %s\n", yytext); }

%%

int yywrap() {
    return 1;
}
