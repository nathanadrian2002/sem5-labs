

%{
#include "q3.tab.h"
%}


%%

[ \t]+          { /* Ignore whitespace */ }
[0-9]+          { yylval.val = atoi(yytext); return NUM; }
"\n"            { return NEWLINE; }
"+"             { return ADD; }
"-"             { return SUB; }
"*"             { return MUL; }
"/"             { return DIV; }
.               { /* Catch all other characters */ yyerror("Invalid character"); return 0; }

%%



int yywrap() {
	return 1;
}
