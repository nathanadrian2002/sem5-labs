%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int yylex();
int yyerror();
%}

%union {
  int val;
}

%token <val> NUM
%token NEWLINE
%token ADD SUB MUL DIV

%type <val> exp
%type <val> line
%type <val> input


%%

input: input line |
    ;

line:
      NEWLINE { /* Do nothing on empty lines */ }
    | exp NEWLINE { printf("Result: %d\n", $1); }
    ;

exp:
      NUM        { $$ = $1; }
    | exp exp ADD { $$ = $1 + $2; }
    | exp exp SUB { $$ = $1 - $2; }
    | exp exp MUL { $$ = $1 * $2; }
    | exp exp DIV {
                      if ($2 == 0)
                        { yyerror("Division by zero."); YYABORT; }
                      else
                        { $$ = $1 / $2; }
                    }
    ;

%%

int yyerror(char *msg)
{
	printf("Invalid Expression\n");
	exit(0);
}
void main ()
{
	printf("Enter the expression\n");
	yyparse();
}

