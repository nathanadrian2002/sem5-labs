%{
#include <stdio.h>
#include <stdlib.h>

int yylex();
int yyerror();
%}

%token IF ELSE ID NUM ASSIGN ADDOP MULOP RELOP
%left ADDOP MULOP RELOP

%%

decision:
	IF '(' expn ')' '{' statement_list '}' decision_else{
		printf("Valid Decision\n");
		exit(0);
	}
;

decision_else:
	ELSE decision_prime
	|
;

decision_prime:
	decision
	| '{' statement_list '}'
;

statement_list:
	statement statement_list
	|
;

statement:
	assignment ';'
	| decision
;

assignment:
	ID ASSIGN expn
;

expn:
	simple_expn eprime
;

simple_expn:
	term seprime
;

term:
	factor tprime
;

tprime:
	MULOP factor tprime
	|
;

seprime:
	ADDOP term seprime
	|
;

eprime:
	RELOP simple_expn
	|
;

factor:
	ID
	| NUM
;



%%

int yyerror(char *msg)
{
	printf("Invalid Expression\n");
	exit(0);
}
void main ()
{
	printf("Enter the expression\n");
	yyparse();
}
