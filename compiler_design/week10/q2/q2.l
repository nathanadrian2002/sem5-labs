

%{
#include "q2.tab.h"
%}

%%

"if"           { return IF; }
"else"         { return ELSE; }
"=="|"!="|"<="|">="|"<"|">"  { return RELOP; }
"+"|"-"       { return ADDOP; }
"\\*"|"/" { return MULOP; }
"="            { return ASSIGN; }
[0-9]+         { yylval = atoi(yytext); return NUM; }
[a-zA-Z_][a-zA-Z0-9_]*  { yylval = strdup(yytext); return ID; }
";"            { return ';'; }
"{"            { return '{'; }
"}"            { return '}'; }
"("            { return '('; }
")"            { return ')'; }
[ \t\n]+       { /* ignore whitespaces */ }
.              { /* unrecognized characters */ }

%%


int yywrap() {
	return 1;
}
