#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../util/util.h"

enum Type{
	
	ARITHMETIC,
	RELATIONAL,
	LOGICAL,
	SPEACIAL_SYMBOL,
	KEYWORD,
	IDENTIFIER,

};

typedef struct Token{
	int row;
	int column;
	char name[20];
	enum Type type;

} Token;


char* get_token_type(enum Type type);
void print_token(Token t);


int is_arithmetic(FILE * file_ptr, int reset_to, Token* t);
int is_relational(FILE* file_ptr, int reset_to, Token* t);
int is_logical(FILE* file_ptr, int reset_to, Token* t);
int is_special_symbol(FILE* file_ptr, int reset_to, Token* t);
int is_keyword(FILE* file_ptr, int reset_to, Token* t);
int is_numeric_constant(FILE* file_ptr, int reset_to, Token* t);
int is_string_literal(FILE* file_ptr, int reset_to, Token* t);
int is_identifier(FILE* file_ptr, int reset_to, Token* t);


void get_next_token(char* file_name, int* l, int* col, int* pos,Token* t);
