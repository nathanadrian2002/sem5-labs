#include<stdio.h>
#include<string.h>
#include "../util/util.h"

void clear_whitespaces(char* input_file, char* output_file);

int add_to_buffer(char* buffer, char c);

void preprocess(char* input_file, char* output_file);