#include "preprocessor.h"

/* void clear_whitespaces(char* input_file, char* output_file)
{
	FILE *in, *out;
	int l = 0, ch = -1, count = 0;
	char c = 0;

	in = fopen(input_file, "r");

	if (in == NULL)
	{
		printf("No Such File\n");
		return;
	}

	printf("FIle found\n");

	out = fopen(output_file, "w+");

	while (c != EOF)
	{
		c = fgetc(in);

		if (c == ' ' || c == '\t')
		{	
			count++;
			c =' ';
		}
		else
			count = 0;

		if (count > 1)
			continue;

		if (c == EOF)
			continue;

		fputc(c, out);
		
	}

	fclose(in);
	fclose(out);
} */

int add_to_buffer(char* buffer, char c)
{
	int i = strlen(buffer);
	
	
	
	if (c == '\t')
		c = ' ';

	if (i == 0 && c == ' ')
		return 0;

	if (c == ' ' && buffer[i-1] == ' ')
		return 0;

	

	strcpy(buffer+i, (char[2]){c, '\0'});

	if (c == '\n')
		return 1;

	if (i == 0)
		return 0;
	
	if (c == ' ' && buffer[i-1] != ' ')
		return 1;
	


	return 0;
}


void preprocess(char* source_filename, char* destination_filename)
{
    FILE *source_file, *destination_file;
	int flag = 0, cf = 0;
    char c = 0;
	char buffer[200] = "";

	char *pre_dir[] = {"#include", "#define", "//"};
	int pre_dir_len = 3;

    source_file = fopen(source_filename, "r");

	if (source_file == NULL)
	{
		printf("Preprocessor Found No File\n");
		return;
	}

    destination_file = fopen(destination_filename, "w+");

    while(1)
    {
		c = fgetc(source_file);
		if (c == EOF)
			break;

		flag = add_to_buffer(buffer, c);
		
		if (flag)
		{
			// printf("Writing ");
			
			for (int p = 0; p < pre_dir_len; p++)
			{	
				if (is_prepened(buffer, pre_dir[p]))
					cf = 1;
			}

			if (is_prepened(buffer, "/*"))
				// printf("Comment Found\n");
				cf = 2;

			for (int i = 0; i < strlen(buffer); i++)
			{
			
				if (cf == 0)
				{
					fputc(buffer[i], destination_file);
					
				}
				if (cf == 1 && buffer[i] == '\n')
					cf = 0;
				if (cf == 2 &&i > 0 && buffer[i] == '/' && buffer[i-1] == '*')
					cf = 0;

			}	
			buffer[0] = '\0';
		}
			
    }

	fclose(source_file);
	fclose(destination_file);

}