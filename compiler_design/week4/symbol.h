#include<string.h>

// #include "token.h"

struct Symbol{
	
	int serial, 
		num_args;

	char lexeme_name[100], 
		 data_type[10], 
		 return_type[10],
		 token_type[10];

	struct Symbol * next;

};

typedef struct Symbol Symbol;

void print_symbols(Symbol* s)
{
	if (s == NULL)
		printf("Symbol Table Is Empty\n");

	else
		printf("Serial\tName\tType\tData\tReturn\tArgs\n");

	while(s != NULL)
	{
		printf("%d\t%s\t%s\t", s->serial, s->lexeme_name, s->token_type);
		
		if (strcmp(s->token_type, "func") == 0)
			printf("%s\t%s\t%d", "---", s->return_type, s->num_args);
		else if (strcmp(s->token_type, "var") == 0)
			printf("%s\t%s \t%s", s->data_type,"---", "--");
		
		printf("\n");

		s = s->next;
	}
}

int table_length(Symbol* s)
{
	int result = 0;

	while(s != NULL)
	{
		result++;
		s = s->next;
	}
	return result;

}

Symbol* create_function_symbol(int serial, char* name, char* return_type, int args)
{
	Symbol* s = (Symbol*)calloc(1, sizeof(Symbol));

	s->serial = serial;

	strcpy(s->lexeme_name, name);

	strcpy(s->token_type, "func");

	strcpy(s->return_type, return_type);

	s->num_args = args;

	return s;
}

Symbol* create_variable_symbol(int serial, char* name, char* data_type)
{
	Symbol* s = (Symbol*)calloc(1, sizeof(Symbol));

	s->serial = serial;

	strcpy(s->lexeme_name, name);

	strcpy(s->token_type, "var");

	strcpy(s->data_type, data_type);

	return s;
}

void copy_symbol(Symbol* dest, Symbol src)
{
	dest->serial = src.serial;
	strcpy(dest->lexeme_name, src.lexeme_name);
	strcpy(dest->token_type, src.token_type);
	strcpy(dest->data_type, src.data_type);
	strcpy(dest->return_type, src.return_type);
	dest->num_args = src.num_args;
}

void clear_symbol(Symbol* s)
{
	s->serial = 0;
	strcpy(s->lexeme_name, "");
	strcpy(s->token_type, "");
	strcpy(s->data_type, "");
	strcpy(s->return_type, "");
	s->num_args = 0;
}

void add_symbol(Symbol** table, Symbol sample)
{
	Symbol* entry = (Symbol*)calloc(1, sizeof(Symbol));
	Symbol *temp = *table; 
	int count = 0;
	copy_symbol(entry, sample);
	
	entry->serial = count;

	if (*table == NULL)
		*table = entry;
	else
	{
		count = 1;
		while (temp->next != NULL)
		{	
			if (strcmp(entry->lexeme_name, temp->lexeme_name) == 0)
				return;
			temp = temp->next;
			count++;
		}
		// printf("Passing by: %s\n", temp->lexeme_name);
		if (strcmp(entry->lexeme_name, temp->lexeme_name) == 0)
			return;
		entry->serial = count;
		temp->next = entry;
	}

}

