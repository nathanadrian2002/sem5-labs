#include<stdio.h>
#include<string.h>
#include<stdlib.h>


#include "preprocessor/preprocessor.h"
#include "token/token.h"
#include "symbol/symbol.h"

// Build with: 
// gcc -o main.o main.c preprocessor/preprocessor.c symbol/symbol.c token/token.c util/util.c


int main()
{

	int position = 0, line = 1, column = 1, is_token;
	char c = 0;

	Token t;
	Symbol * symbol_table = NULL, s;
	
	clear_symbol(&s);
	print_symbols(symbol_table);

	preprocess("test.c", "t.temp");

	get_next_token("t.temp", &line, &column, &position, &t);
	
	while(strcmp(t.name,"") != 0)
	{
		print_token(t);
		update_symbol_table(t, &s, &symbol_table);
		get_next_token("t.temp", &line, &column, &position, &t);
	}

	print_symbols(symbol_table);

}