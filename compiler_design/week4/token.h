
enum Type{
	ARITHMETIC,
	RELATIONAL,
	LOGICAL,
	SPEACIAL_SYMBOL,
	KEYWORD,
	IDENTIFIER,

};

typedef struct Token{
	int row;
	int column;
	char name[20];
	enum Type type;

} Token;


void print_token(Token t){
	// printf("Token: %s\n", t.type);
    if (t.type == IDENTIFIER)
		printf("< %s , r : %d , c : %d >\n", "ID", t.row, t.column);
	else
	printf("< %s , r : %d , c : %d >\n", t.name, t.row, t.column);
}
