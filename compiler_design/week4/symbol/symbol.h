#pragma once

#include<string.h>
#include<stdlib.h>

#include "../token/token.h"

struct Symbol{
	
	int serial, 
		num_args;

	char lexeme_name[100], 
		 data_type[10],
		 return_type[10],
		 token_type[10];

	struct Symbol * next;

};

typedef struct Symbol Symbol;

void print_symbols(Symbol* s);
int table_length(Symbol* s);

void copy_symbol(Symbol* dest, Symbol src);
void clear_symbol(Symbol* s);

void add_symbol(Symbol** table, Symbol sample);

void update_symbol_table(Token token, Symbol* cache, Symbol** table);
