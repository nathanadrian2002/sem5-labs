#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Token{
    char name[50];
};
typedef struct Token Token;

FILE * fp;
Token token;

void error()
{
    printf("Invalid Token\n");
    exit(0);
}

Token get_next_token()
{
    char buffer[100] = "";
    int i = 0;
    char c = fgetc(fp);

    Token token = { "" };

    while (c == ' ' || c == '\n' || c == '\t')
        c = fgetc(fp);
    
    if (c == EOF)
        return token;
    // printf("Wowo");
    while (c != ' ' && c != EOF)
    {
        buffer[i++] = c;
        c = fgetc(fp);
    }

    strcpy(token.name, buffer);
    buffer[0] = '\0';
    printf("Token: %s\n", token.name);
    return token;

}



/*

S -> NP VP
NP -> Det Adj N | Pro
V -> Vi | Vt | Vp
VP -> V | V NP | V NP PP
PP -> Prep NP

Adj -> 'small' | 'red' | 'old' | 'young'
Det -> 'a' | 'an' | 'the'
Pro -> 'they' | 'we' | 'you' | 'he' | 'she'
N -> 'dog' | 'cat' | 'apple' | 'house'

Prep -> 'above' | 'below' | 'near'
Vi -> 'sleeps' | 'jumps' | 'talks'
Vt -> 'found' | 'lost' | 'threw'
Vp -> 'running' | 'sleeping' | 'talking'

*/

int Adj()
{
    if (strcmp(token.name, "small") == 0)
        ;
    else if (strcmp(token.name, "red") == 0)
        ;
    else if (strcmp(token.name, "old") == 0)
        ;
    else if (strcmp(token.name, "young") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Det()
{
    if (strcmp(token.name, "a") == 0)
        ;
    else if (strcmp(token.name, "an") == 0)
        ;
    else if (strcmp(token.name, "the") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Pro()
{
    if (strcmp(token.name, "they") == 0)
        ;
    else if (strcmp(token.name, "we") == 0)
        ;
    else if (strcmp(token.name, "you") == 0)
        ;
    else if (strcmp(token.name, "he") == 0)
        ;
    else if (strcmp(token.name, "she") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int N()
{
    if (strcmp(token.name, "dog") == 0)
        ;
    else if (strcmp(token.name, "cat") == 0)
        ;
    else if (strcmp(token.name, "apple") == 0)
        ;
    else if (strcmp(token.name, "house") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Prep()
{
    if (strcmp(token.name, "above") == 0)
        ;
    else if (strcmp(token.name, "below") == 0)
        ;
    else if (strcmp(token.name, "near") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Vi()
{
    if (strcmp(token.name, "sleeps") == 0)
        ;
    else if (strcmp(token.name, "jumps") == 0)
        ;
    else if (strcmp(token.name, "talks") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Vt()
{
    if (strcmp(token.name, "found") == 0)
        ;
    else if (strcmp(token.name, "lost") == 0)
        ;
    else if (strcmp(token.name, "threw") == 0)
        ;
    else
        return 0;
    
    return 1;
}
int Vp()
{
    if (strcmp(token.name, "running") == 0)
        ;
    else if (strcmp(token.name, "sleeping") == 0)
        ;
    else if (strcmp(token.name, "talking") == 0)
        ;
    else
        return 0;
    
    return 1;
}

int NP()
{
    token = get_next_token();
    if (Det())
    {
        printf("Det Done\n");

        token = get_next_token();
        if (!Adj())
            error();

        token = get_next_token();
        if (!N())
            error();
    }
    else if (Pro())
    {

    }
    else 
        return 0;
    return 1;
}

int V()
{
    token = get_next_token();

    if (Vi())
        ;
    else if (Vp())
        ;
    else if (Vt())
        ;
    else
        return 0;
    return 1;
}

int PP()
{
    token = get_next_token();
    if (!Prep())
        return 0;
    token = get_next_token();
    
    if (!NP())
        return 0;
    return 1;

}

int VP()
{
    if (!V())
        return 0;
    printf("V Done\n");

    if (NP())
    {
        printf("NP Done\n");
        if (PP())
            ;
    }

    return 1;
}

int S()
{
    if (!NP())
        error();

    printf("NP Done\n");

    if (!VP())
        error();

    return 1;
}




int main()
{
    fp = fopen("input.txt", "r");
    
    if (S())
        printf("Valid Parsing\n");
    else
        printf("Invalid Parsing\n");

    // 
    // printf("Start\n");
    // do
    // {
    //     token = get_next_token();
    //     // printf("Token\n");
    // }while(strcmp(token.name, "") != 0);

    return 0;

}
