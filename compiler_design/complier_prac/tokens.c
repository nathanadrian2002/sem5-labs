#include<stdio.h>
#include<stdlib.h>
#include<string.h>

FILE *fp;

struct Token{
    char name[10];
};
typedef struct Token Token;

Token current;


Token get_token(FILE ** fp)
{
    char c = fgetc(*fp);
    int i = 0;
    char buffer[50] = "";
    
    Token token = {""};

    while(c == ' ' || c == '\t' || c == '\n')
        c = fgetc(*fp);
    
    if (c == EOF)
    {
        buffer[0] = '\0';
        return token;
    }
    // printf("WOw\n");
    // buffer[0]=c;

    while (c != ' ' && c != '\t' && c!= EOF && c != '\n')
    {
        buffer[i++] = c;
        c = fgetc(*fp);
    }    

    strcpy(token.name, buffer);

    if (c == EOF)
    {
        buffer[0] = '\0';
    }
    printf("TOken: %s\n", token.name);
    return token;
}

void error()
{
    printf("Syntax Error\n");
    exit(0);
}

// void S()
// {
//     NP();
//     VP();
// }
// int Det()
// {
//     return Art();
// }
// int NP()
// {
//     if (Pro())
//         ;
//     else if (Det())
//         N();
//     else if (N())
//         ;
//     else
//         error();
//     return 1;
// }
// void VP()
// {
//     V();
//     if (NP())
//     {
//         if (PP())
//             ;
//     }
// }
// int PP()
// {
//     Prep();
//     NP();
// }

int Pro()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "i") == 0);
    else if ( strcmp(current.name, "we") == 0);
    else if ( strcmp(current.name, "you") == 0);
    else if ( strcmp(current.name, "he") == 0);
    else if ( strcmp(current.name, "she") == 0);
    else if ( strcmp(current.name, "him") == 0);
    else if ( strcmp(current.name, "her") == 0);
    else    
        return 0;
    return 1;
}

int Art()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "a") == 0);
    else if ( strcmp(current.name, "an") == 0);
    else if ( strcmp(current.name, "the") == 0);
    else    
        return 0;
    return 1;
}

int Prep()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "in") == 0);
    else if ( strcmp(current.name, "with") == 0);
    else    
        return 0;
    return 1;
}


int N()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "salad") == 0);
    else if ( strcmp(current.name, "mushrooms") == 0);
    else if ( strcmp(current.name, "fork") == 0);
    else    
        return 0;
    return 1;
}

int V()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "eats") == 0);
    else if ( strcmp(current.name, "eat") == 0);
    else if ( strcmp(current.name, "ate") == 0);
    else if ( strcmp(current.name, "see") == 0);
    else if ( strcmp(current.name, "saw") == 0);
    else if ( strcmp(current.name, "prefer") == 0);
    else if ( strcmp(current.name, "sneezed") == 0);
    else    
        return 0;
    // printf("Passed V\n");
    return 1;
}


int Vi()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "ran") == 0);
    else if ( strcmp(current.name, "sneezed") == 0);
    else    
        return 0;
    return 1;
}

int Vt()
{
    // current = get_token(&fp);

    if ( strcmp(current.name, "eats") == 0);
    else if ( strcmp(current.name, "eat") == 0);
    else if ( strcmp(current.name, "ate") == 0);
    else if ( strcmp(current.name, "see") == 0);
    else if ( strcmp(current.name, "saw") == 0);
    else if ( strcmp(current.name, "prefer") == 0);
    else    
        return 0;
    return 1;
}



int NP()
{
    if (Pro())
        ;
    else if (Art())
    {
        current = get_token(&fp);
        if (!N())
            error();
    }
    else if (N())
        ;   
    else
        return 0;
    // printf("Passed NP\n");
    return 1;
}


int PP()
{
    if (!Prep())
        return 0;
    current = get_token(&fp);

    if (!NP())
        return 0;

    return 1;

}

int VP()
{
    if (!V())
        error();
    current = get_token(&fp);

    if (NP())
    {
        // printf("Entering\n");
        current = get_token(&fp);

        if (PP())
            current = get_token(&fp);

    }
    // printf("Passeed VP\n");
    return 1;
}

int S()
{
    current = get_token(&fp);
    if (! NP())
        error();
    
    current = get_token(&fp);
    
    if (!VP())
        error();

    return 1;
}


int main()
{
    
    
    Token token;

    fp = fopen("input.txt", "r");


    // do{
        
    //     // token = get_token(&fp);
    //     printf("Token: %s\n", token.name);
    
    // }while (strcmp(token.name, "") != 0);
    
    if (S())
        printf("Vaild Parsing\n");
    else
        printf("INVALID Parsing\n");

    return 0;
}