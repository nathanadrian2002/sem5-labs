%{
#include <stdio.h>
#include <string.h>

extern int yylex();
extern int yyparse();
extern FILE *yyin;
extern int yylineno; // line number
extern char* yytext; // current token text

void yyerror(const char *s);
%}

%union {
    char *str;
    int ival;
}

%token <str> ID
%token <ival> MODULE INPUT OUTPUT INOUT ASSIGN WIRE ENDMODULE
%token NAND NOR XOR OR AND

// Operator precedence and associativity (adjust based on your operators)
%left '+' '-'
%left '*' '/'  // Add these if multiplication and division are part of your operators
%left '&' '|'
%left '^'
%right '~'  // Unary operator
%nonassoc '?' ':'  // For ternary conditional operator


%%

program:
    MODULE ID '(' id_list ')' ';' module_items ENDMODULE
    ;

module_items:
    /* empty */
    | module_item module_items
    ;

module_item:
    declaration
    | instantiation
    | assignment
    ;

declaration:
    port_direction id_list ';'
    ;

port_direction:
    INPUT
    | OUTPUT
    | INOUT
    | WIRE
    ;

instantiation:
    ID ID '(' connections_or_ids ')' ';'
    ;

connections_or_ids:
    connections
    | id_list
    ;

connections:
    '.' ID '(' ID ')' connections_tail
    ;

connections_tail:
    /* empty */
    | connections_tail ',' '.' ID '(' ID ')'
    ;

assignment:
    ASSIGN ID '=' expr ';'
    | logical_gate '(' ID ',' ID ',' ID ')' ';'
    ;

logical_gate:
    NAND
    | NOR
    | XOR
    | OR
    | AND
    ;

expr:
    ID '?' expr ':' expr
    | term expr_tail
    ;

expr_tail:
    /* empty */
    | binary_op term expr_tail
    ;

term:
    unary_expr
    | '(' expr ')'
    | ID
    ;

unary_expr:
    unary_op term
    ;

binary_op:
    '&'
    | '|'
    | '^'
    | '+'
    | '-'
    ;

unary_op:
    '~'
    ;

id_list:
    ID id_list_tail
    ;

id_list_tail:
    /* empty */
    | id_list_tail ',' ID
    ;


%%

void yyerror(const char *s) {
    if(yytext){
        printf("Syntax error at line %d near token '%s': %s\n", yylineno, yytext, s);
    } else {
        printf("Syntax error at line %d: %s\n", yylineno, s);
    }
}

int main(int argc, char **argv) {
    yyin = fopen(argv[1], "r");
    if (!yyin) {
        printf("Error opening file: %s\n", argv[1]);
        return 1;
    }

    if (yyparse() == 0) {
        printf("Parsing completed successfully!\n");
    } else {
        printf("Parsing encountered errors.\n");
    }


    fclose(yyin);
    return 0;
}
