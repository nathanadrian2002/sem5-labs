%{
#include "verilog_parser.tab.h"
#include <stdio.h>

int print_tokens = 1; // When set to 1, tokens will be printed.
%}

%%

[ \t\n]           { /* Ignore whitespace */ }

"module"          { if(print_tokens) printf("Token: MODULE\n"); return MODULE; }
"input"           { if(print_tokens) printf("Token: INPUT\n"); return INPUT; }
"output"          { if(print_tokens) printf("Token: OUTPUT\n"); return OUTPUT; }
"inout"           { if(print_tokens) printf("Token: INOUT\n"); return INOUT; }
"assign"          { if(print_tokens) printf("Token: ASSIGN\n"); return ASSIGN; }
"wire"            { if(print_tokens) printf("Token: WIRE\n"); return WIRE; }
"endmodule"       { if(print_tokens) printf("Token: ENDMODULE\n"); return ENDMODULE; }
"nand"          { if(print_tokens) printf("Token: NAND\n"); return NAND; }
"nor"           { if(print_tokens) printf("Token: NOR\n"); return NOR; }
"xor"           { if(print_tokens) printf("Token: XOR\n"); return XOR; }
"or"            { if(print_tokens) printf("Token: OR\n"); return OR; }
"and"           { if(print_tokens) printf("Token: AND\n"); return AND; }

[A-Za-z_][A-Za-z0-9_]* {
                    if(print_tokens) printf("Token: ID, Value: %s\n", yytext);
                    yylval.str = strdup(yytext);
                    return ID;
                  }

[()=.,;&:?^|+-~]   { if(print_tokens) printf("Token: SYMBOL, Value: %c\n", *yytext); return *yytext; }

.                 { printf("Unrecognized character: %c\n", *yytext); exit(1); }

%%

int yywrap() {
    return 1;
}
