module ALU(A, B, S, Y);
input A, B, S ;
output Y;
wire W1, W2;
ALU_submodule U2 (.A(A), .B(B), .Y(W2));
ALU_submodule U2 (.A(A), .B(B), .Y(W2));

assign Y = S ? W1 + j : W2;
assign Y = S ? W1 + j : W2;

assign I = ~ ( I + R ) ;

xor(A, B, C);

endmodule

