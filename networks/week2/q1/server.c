#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#define PORTNO 10200

void print_array(int* arr , int n)
{
	for (int i = 0; i < n; i ++)
		printf("%d  ", arr[i]);
	printf("\n");
}
void sort(int* arr, int n)
{
	int t;
	// print_array(arr, n);
	for (int i = 0; i < n; i++)
	{
		for (int j = 1; j < n-i; j++)
		{
			if (arr[j] < arr[j-1])
			{
				t = arr[j];
				arr[j] = arr[j-1];
				arr[j-1] = t;
			}

		}
	}

	// print_array(arr, n);

}

void compute(char * buf)
{
	int n = buf[0] - '0';
	int j = 0;
	int * arr = (int*)calloc(sizeof(int), n);

	for (int i = 0; i < n; i++)
	{
		while(buf[j] == ' ')
			j++;
		arr[i] = buf[j++] - '0';
	}

	sort(arr, n);
	j = 0;
	for (int i = 0; i < n; i++)
	{
		buf[j++] = arr[i] + '0';
		buf[j++] = ' ';
		buf[j] = '\0';
	}
	// printf("")
	n = getpid();


	printf("Pid is: %d", n);
	sprintf(buf + j, "%s", "PID: "); j +=5;
	sprintf(buf + j, "%d", n);

	printf("\n");
	// return buf;
}

int main()
{
	int sockfd,newsockfd,portno,clilen,n=1;
	char buf[256];
	struct sockaddr_in seraddr,cliaddr;
	int i,value;
	
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	seraddr.sin_family = AF_INET;
	seraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	seraddr.sin_port = htons(PORTNO);
	bind(sockfd,(struct sockaddr *)&seraddr,sizeof(seraddr));
	
	// Create a connection queue, ignore child exit details, and wait for clients
	listen(sockfd,5);
	
	while(1){
		
		//Accept the connection
		clilen = sizeof(clilen);
		newsockfd=accept(sockfd,(struct sockaddr *)&cliaddr,&clilen);
		
		//Fork to create a process for this client and perform a test to see whether
		//you’re the parent or the child:
		if(fork()==0){
		
			// If you’re the child, you can now read/write to the client on newsockfd.
			n = read(newsockfd,buf,sizeof(buf));

			compute(buf);
			printf(" \nMessage from Client %s \n",buf);

			n = write(newsockfd,buf,sizeof(buf));
			
			close(newsockfd);
			exit(0);
		}
		
		//Otherwise, you must be the parent and your work for this client is finished
		else
			close(newsockfd);
	}
}