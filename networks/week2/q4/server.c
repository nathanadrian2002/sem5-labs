#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<time.h>
#define PORTNO 10200

void compute(char * buf)
{
	char word[100], twoD[10][30];

	int i = 0, j = 0, k = 0,d=0, len1 = 0, len2 = 0, l = 0;


	// let us convert the string into 2D array

	for (i = 0; buf[i] != '\0'; i++)
	{

		if (buf[i] == ' ')
		{

			twoD[k][j] = '\0';

			k ++;

			j = 0;

		}

		else
		{

			twoD[k][j] = buf[i];

			j ++;

		}

	}


	twoD[k][j] = '\0';

 
	j = 0;

	for (i = 0; i < k; i++)

	{

		int present = 0;

		for (l = 1; l < k + 1; l++)

		{

			if (twoD[l][j] == '\0' || l == i)

			{

				continue;

			}

			if (strcmp (twoD[i], twoD[l]) == 0) {

				twoD[l][j] = '\0';

				present = present + 1;
			}
		}

	}

 

	j = 0;

 	buf[0] = '\0';
 	
	for (i = 0; i < k + 1; i++)

	{

		if (twoD[i][j] == '\0')

			continue;

		else
		{
			d = strlen(buf);
			strcpy(buf + d, twoD[i]);
			d = strlen(buf);
			strcpy(buf+d, " ");
		}

	}

 
}


int main()
{

	int sockfd,newsockfd,portno,clilen,n=1;
	char buf[256];
	struct sockaddr_in seraddr,cliaddr;
	int i,value;
	
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	seraddr.sin_family = AF_INET;
	seraddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	seraddr.sin_port = htons(PORTNO);
	bind(sockfd,(struct sockaddr *)&seraddr,sizeof(seraddr));
	
	// Create a connection queue, ignore child exit details, and wait for clients
	listen(sockfd,5);
	
	while(1){
		
		//Accept the connection
		clilen = sizeof(clilen);
		newsockfd=accept(sockfd,(struct sockaddr *)&cliaddr,&clilen);
		
		//Fork to create a process for this client and perform a test to see whether
		//you’re the parent or the child:
		if(fork()==0){
		
			// If you’re the child, you can now read/write to the client on newsockfd.
			n = read(newsockfd,buf,sizeof(buf));

			compute(buf);
			printf(" \nMessage to Client %s \n",buf);

			n = write(newsockfd,buf,sizeof(buf));
			
			close(newsockfd);
			exit(0);
		}
		
		//Otherwise, you must be the parent and your work for this client is finished
		else
			close(newsockfd);
	}
}