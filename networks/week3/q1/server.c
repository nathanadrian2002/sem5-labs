#include<stdio.h>
#include<fcntl.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>

#define PORTNO 10201

void print_me()
{
	printf("Name: Nathan Adrina Saldanha\n");
	printf("Registration: 210905162\n");
	printf("Roll No. 34\n");
}
// void gen_pattern(int n, char * pattern)
// {
// 	pattern[0] = '\0';
// 	for (int i = 0; i < n; i++)
// 	{
// 		strcat(pattern, "%d ");
// 	}
// 	pattern[n*3] = '\0';

// }

int main()
{
	print_me();
	
	int sd;
	char buf[25];
	struct sockaddr_in sadd,cadd;
	
	int size, *arr;
	char pattern[100];

	//Create a UDP socket
	sd=socket(AF_INET,SOCK_DGRAM,0);
	
	//Construct the address for use with sendto/recvfrom... */
	sadd.sin_family=AF_INET;
	sadd.sin_addr.s_addr=inet_addr("127.0.0.1");//**
	sadd.sin_port=htons(PORTNO);
	int len = sizeof(cadd);
	int result = bind(sd,(struct sockaddr *)&sadd,sizeof(sadd));
	
	int m = recvfrom(sd, buf, sizeof(buf), 0, (struct sockaddr *)&cadd, &len);
	sscanf(buf,"%d", &size);
	printf("Matrix size is: %d\n", size);
	// gen_pattern(size, pattern);
	int n=sendto(sd,buf,sizeof(buf),0,(struct sockaddr *)&cadd,len);
	
	printf("The Matrix is: \n");
	for (int row= 0; row < size; row++)
	{
		m = recvfrom(sd, buf, sizeof(buf), 0, (struct sockaddr *)&cadd, &len);
		printf("%s\n", buf);
		n = sendto(sd,buf,sizeof(buf),0,(struct sockaddr *)&cadd,len);

	}

	return 0;
}