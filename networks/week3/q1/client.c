#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<unistd.h>

#define PORTNO 10201

void print_me()
{
	printf("Name: Nathan Adrina Saldanha\n");
	printf("Registration: 210905162\n");
	printf("Roll No. 34\n");
}

void arr_to_buffer(int * arr, int n, char * buf)
{
	buf[0] = '\0';
	for (int i = 0; i < n; i++)
		sprintf(buf + strlen(buf), "%d ", arr[i]);
	

}

int main()
{
	print_me();
	int sd;
	struct sockaddr_in address;
	
	int n, *arr;

	sd=socket(AF_INET,SOCK_DGRAM,0);
	address.sin_family=AF_INET;
	address.sin_addr.s_addr=inet_addr("127.0.0.1");//**
	address.sin_port=htons(PORTNO);
	int len=sizeof(address);

	char buf[25],buf1[25];
	printf("Enter size of matrix: ");
	scanf("%d", &n);
	
	sprintf(buf, "%d", n);

	int sen = sendto(sd,buf,sizeof(buf),0,(struct sockaddr *)&address, len);
	int rec = recvfrom(sd,buf,sizeof(buf),0,(struct sockaddr *)&address,&len);

	for (int send = 0; send < n; send++)
	{
		printf("Enter Values in Row %d: ", send+1);
		for (int c = 0; c < n; c++)
			scanf("%d", arr + c);

		arr_to_buffer(arr, n, buf);

		sen = sendto(sd,buf,sizeof(buf),0,(struct sockaddr *)&address, len);
		rec = recvfrom(sd,buf,sizeof(buf),0,(struct sockaddr *)&address,&len);

	}	
	
	return 0;
}