#include <pthread.h>
#include <stdio.h>

int fib(n)
{

	if ((n == 0) || (n == 1))
		return 1;

	return fib(n-1) + fib(n-2);
}

void* child_thread( void * param )
{
	int id = fib((int)param);
	
	return (void *)id;
}

int main()
{
	int n;
	pthread_t* threads;
	int *return_value;

	printf("Enter N: "); 
	// n = 6;
	scanf("%d", &n);
	

	threads = (pthread_t*) calloc(n , sizeof(pthread_t));
	return_value = (int*) calloc(n , sizeof(int));

	for (int i = 0; i < n; i ++)
	{
		pthread_create( threads+i, 0, &child_thread, (void*)i );
	}

	for ( int i=0; i<n; i++ )
	{
		pthread_join( threads[i], (void**)(return_value+i) );
	}

	// Result
	for (int i = 0; i < n; i++)
		printf("%d  ", return_value[i]);
	printf("\n");

}