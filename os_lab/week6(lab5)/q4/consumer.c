#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#define FN "/tmp/my_fifo"
#define BS 10
// #define TEN_MEG (1024 * 1024 * 10)

int main()
{
	int pf;
	int res;
	int om = O_RDWR;
	int s = 0;

	char b[BS + 1];

	printf("P: %d opening FIFO Write Only\n", getpid());
	
	pf = open(FN, om);
	res = write(pf, "0", strlen("0"));


	while(1) {
		

		res = read(pf, b, BS);
		
		if (res == -1) {
			break;
		}

		s = stoi(b);

		if (s >= 5)
			continue;
		else
			s++;

		sprintf(b, "%d", s);
		res = write(pf, b, BS);
		
		if (res == -1) {
			break;
		}
		
	}

	(void)close(pf);

	
	printf("P: %d finished\n", getpid());
	return 0;
}