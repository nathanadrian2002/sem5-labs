#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#define FN "/tmp/my_fifo"
#define BS PIPE_BUF

int main()
{
	int pf;
	int res;
	int om = O_RDONLY;
	char b[BS + 1];
	int i = 0;
	memset(b, '\0', sizeof(b));
	printf("P: %d opening FIFO Read Only\n", getpid());
	pf = open(FN, om);
	printf("P: %d result %d\n", getpid(), pf);
	
	if (pf != -1){
		
		do{
			res = read(pf, b, BS);
			i+=res;
			if (res == 0) break;
			printf("%s ", b);
		} while (1);
		printf("\n");
		(void)close(pf);
	}
	else
		return -1;

	printf("P: %d finished, %d bytes read\n", getpid(), i);
	return 0;
}