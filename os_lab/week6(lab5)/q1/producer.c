#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#define FN "/tmp/my_fifo"
#define BS PIPE_BUF
// #define TEN_MEG (1024 * 1024 * 10)

int main()
{
	int pf;
	int res;
	int om = O_WRONLY;
	int is = 0;
	int i[] = {7, 8, 6, 3};
	char buf[BS + 1];
	
	if (access(FN, F_OK) == -1) {
		res = mkfifo(FN, 0777);
		if (res != 0) {
			fprintf(stderr, "Could not create fifo %s\n", FN);
			exit(EXIT_FAILURE);
		}
	}

	printf("P: %d opening FIFO Write Only\n", getpid());
	pf = open(FN, om);

	if (pf != -1) {
		
		while(is < 4) {
			sprintf(buf, "%d", i[is]);
			res = write(pf, buf, BS);
			
			if (res == -1) {
				fprintf(stderr, "Pipe error\n");
				exit(EXIT_FAILURE);
			}
			
			is++;
		}

		(void)close(pf);
	}
	else 
		return -1;
	
	printf("P: %d finished\n", getpid());
	return 0;
}