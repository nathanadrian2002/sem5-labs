#include <sys/wait.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int pfd[2];
	pid_t cpid;
	char buf;
	assert(argc == 2);
	
	if (pipe(pfd) == -1) return -1;
	

	cpid = fork();
	if (cpid == -1) return -1;

	if (cpid == 0) { /* Child reads from pipe */
		close(pfd[1]);
		/* Close unused write end */
		while (read(pfd[0], &buf, 1) > 0)
			printf("%c", buf);
			// write(STDOUT_FILENO, &buf, 1);
		printf("\n");
		close(pfd[0]);
		return 0;
	} 
	else {
		close(pfd[0]);
		write(pfd[1], argv[1], strlen(argv[1]));
		close(pfd[1]);
		wait(NULL);
		return 0;
	}
}