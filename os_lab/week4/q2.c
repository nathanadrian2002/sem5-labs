// Write a program to print out the 
// complete stat structure of a file.

#include<stdio.h>
#include<sys/stat.h>
#include<sys/types.h>

int main(int argc, char *argv[])
{

	struct stat f;
	int r;

	if (argc < 2) {
		fprintf (stderr, "No Args\n");
		return 1;
	}

	r = stat (argv[1], &f);
	
	if (r) {
		perror ("stat");
		return 1;
	}

	printf("File Name: %s\n", argv[1]);
	
	printf("Device Id: %ld\n", f.st_dev);

	printf("Inode Number:%ld\n", f.st_ino);

	printf("Permissions: %d\n", f.st_mode);

	printf("Num hardlinks: %ld\n", f.st_nlink);

	printf("Owner id: %d\n", f.st_uid);

	printf("Group id: %d\n", f.st_gid);

	printf("Device id: %ld\n", f.st_rdev);

	printf("Size(bytes): %ld\n", f.st_size);

	printf("Blocksize: %ld\n", f.st_blksize);

	printf("Num block allocated: %ld\n", f.st_blocks);

	printf("Last access time: %ld\n", f.st_atime);

	printf("last modification time: %ld\n",  f.st_mtime); 
	printf("last status change time: %ld\n",  f.st_ctime);



	return 0;
}