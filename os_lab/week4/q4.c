// Write a program to create a new hardlink
// to an existing file and unlink the same.
// Accept the old path as input and print 
// the newpath.

#include<stdio.h>
#include <unistd.h>
#include<sys/stat.h>
#include<sys/types.h>

int main(int argc, char *argv[])
{

	struct stat f;
	int r;

	if (argc < 3) {
		fprintf (stderr, "No Args\n");
		return 1;
	}

	r = stat (argv[1], &f);
	
	if (r) {
		perror ("stat");
		return 1;
	}

	r = symlink(argv[1], argv[2]);
	
	if (r)
		perror ("link");

	r = unlink(argv[1]);

	if (r)
		perror ("unable to remove");

	return 0;
}