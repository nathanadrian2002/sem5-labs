// Write a program to find the inode number of an 
// existing file in a directory. Take the input as 
// a filename and print the inode number of the file.

#include<stdio.h>
#include<sys/stat.h>
#include<sys/types.h>

int main(int argc, char *argv[])
{

	struct stat f;
	int r;

	if (argc < 2) {
		fprintf (stderr, "No Args\n");
		return 1;
	}

	r = stat (argv[1], &f);
	
	if (r) {
		perror ("stat");
		return 1;
	}

	printf("File Name: %s\nInode Number:%ld", argv[1], f.st_ino);
	return 0;
}
