


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define NUM_REQUESTS 5 // Define the number of requests

typedef struct DSA {
    int request_id;
    int arrival_time_stamp;
    int cylinder;
    int address;
    int process_id;
} DSA;


// Function to sort the array based on cylinder number
int cmpfunc(const void * a, const void * b) {
   return ( ((DSA*)a)->cylinder - ((DSA*)b)->cylinder );
}

// Function to find the index of the request with the minimum seek time
int findMinSeek(int start, DSA requests[], bool visited[], int n) {
    int min_seek = 1000000; // A large number
    int index = -1;
    for (int i = 0; i < n; i++) {
        if (!visited[i] && abs(requests[i].cylinder - start) < min_seek) {
            min_seek = abs(requests[i].cylinder - start);
            index = i;
        }
    }
    return index;
}

// FCFS Disk Scheduling Algorithm
void fcfs(DSA requests[], int n) {
    printf("FCFS Order:\n");
    for (int i = 0; i < n; i++) {
        printf("Request ID: %d, Cylinder: %d\n", requests[i].request_id, requests[i].cylinder);
    }
}

// SSTF Disk Scheduling Algorithm
void sstf(DSA requests[], int n, int initial_pos) {
    printf("\nSSTF Order:\n");
    bool visited[NUM_REQUESTS] = {0};
    int pos = initial_pos;
    for (int i = 0; i < n; i++) {
        int index = findMinSeek(pos, requests, visited, n);
        if (index == -1) // If no unvisited requests left
            break;
        visited[index] = true;
        pos = requests[index].cylinder;
        printf("Request ID: %d, Cylinder: %d\n", requests[index].request_id, pos);
    }
}

// SCAN Disk Scheduling Algorithm
void scan(DSA requests[], int n, int initial_pos) {
    printf("\nSCAN Order:\n");
    qsort(requests, n, sizeof(DSA), cmpfunc); // Sort based on cylinder
    int index = 0;
    // Find start index for scanning
    while (index < n && requests[index].cylinder < initial_pos) {
        index++;
    }
    // Move towards the end
    for (int i = index; i < n; i++) {
        printf("Request ID: %d, Cylinder: %d\n", requests[i].request_id, requests[i].cylinder);
    }
    // Move towards the start
    for (int i = index - 1; i >= 0; i--) {
        printf("Request ID: %d, Cylinder: %d\n", requests[i].request_id, requests[i].cylinder);
    }
}

int main() {
    DSA requests[NUM_REQUESTS] = {
        {1, 0, 98, 0, 1},
        {2, 1, 183, 0, 2},
        {3, 2, 37, 0, 3},
        {4, 3, 122, 0, 4},
        {5, 4, 14, 0, 5},
    };

    int initial_head_position = 53; // Starting position of the disk head

    // Simulate FCFS
    fcfs(requests, NUM_REQUESTS);

    // Simulate SSTF
    sstf(requests, NUM_REQUESTS, initial_head_position);

    // Simulate SCAN
    scan(requests, NUM_REQUESTS, initial_head_position);

    return 0;
}


/*
    
    FCFS Order:
    Request ID: 1, Cylinder: 98
    Request ID: 2, Cylinder: 183
    Request ID: 3, Cylinder: 37
    Request ID: 4, Cylinder: 122
    Request ID: 5, Cylinder: 14

    SSTF Order:
    Request ID: 3, Cylinder: 37
    Request ID: 5, Cylinder: 14
    Request ID: 1, Cylinder: 98
    Request ID: 4, Cylinder: 122
    Request ID: 2, Cylinder: 183

    SCAN Order:
    Request ID: 1, Cylinder: 98
    Request ID: 4, Cylinder: 122
    Request ID: 2, Cylinder: 183
    Request ID: 3, Cylinder: 37
    Request ID: 5, Cylinder: 14


*/