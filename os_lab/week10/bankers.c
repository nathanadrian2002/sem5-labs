#include <stdio.h>

#define N 5  // Number of processes
#define M 3  // Number of resource types

// Function to calculate the Need matrix
void calculateNeed(int need[N][M], int max[N][M], int alloc[N][M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            need[i][j] = max[i][j] - alloc[i][j];
        }
    }
}

// Function to print the matrix
void printMatrix(int matrix[N][M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

// Function to check if the system is in a safe state
int isSafe(int avail[], int max[][M], int alloc[][M], int need[][M]) {
    int work[M];
    for (int i = 0; i < M; i++) {
        work[i] = avail[i];
    }

    int finish[N] = {0};

    while (1) {
        int found = 0;
        for (int p = 0; p < N; p++) {
            if (!finish[p]) {
                int j;
                for (j = 0; j < M; j++)
                    if (need[p][j] > work[j])
                        break;

                if (j == M) {
                    for (int k = 0; k < M; k++)
                        work[k] += alloc[p][k];
                    finish[p] = 1;
                    found = 1;
                }
            }
        }

        if (!found) {
            for (int i = 0; i < N; i++) {
                if (!finish[i]) {
                    return 0;
                }
            }
            break;
        }
    }
    return 1;
}

// Function to process a resource request
int requestResources(int process_num, int request[], int avail[], int alloc[][M], int max[][M], int need[][M]) {
    for (int i = 0; i < M; i++) {
        if (request[i] > need[process_num][i]) {
            printf("Error: P%d has exceeded its maximum claim.\n", process_num);
            return 0;
        }
    }

    for (int i = 0; i < M; i++) {
        if (request[i] > avail[i]) {
            printf("Resources are not available.\n");
            return 0;
        }
    }

    for (int i = 0; i < M; i++) {
        avail[i] -= request[i];
        alloc[process_num][i] += request[i];
        need[process_num][i] -= request[i];
    }

    if (!isSafe(avail, max, alloc, need)) {
        for (int i = 0; i < M; i++) {
            avail[i] += request[i];
            alloc[process_num][i] -= request[i];
            need[process_num][i] += request[i];
        }
        printf("Request cannot be granted as it leads to an unsafe state.\n");
        return 0;
    }
    printf("Request can be granted.\n");
    return 1;
}

int main() {
    int max[N][M];
    int alloc[N][M];
    int need[N][M];
    int avail[M] = {10, 5, 7};

    printf("Enter the Max Matrix for each process: \n");
    for (int i = 0; i < N; i++) {
        printf("Process %d : ", i);
        for (int j = 0; j < M; j++) {
            scanf("%d", &max[i][j]);
        }
    }

    printf("Enter the Allocation Matrix for each process: \n");
    for (int i = 0; i < N; i++) {
        printf("Process %d : ", i);
        for (int j = 0; j < M; j++) {
            scanf("%d", &alloc[i][j]);
        }
    }

    // Calculate initial Need Matrix
    calculateNeed(need, max, alloc);
    printf("The Need Matrix is:\n");
    printMatrix(need);

    // Check if the system is initially in a safe state
    int safe = isSafe(avail, max, alloc, need);
    printf("System is %s in a safe state.\n", safe ? "" : "not");

    // Process request from P1 for (1, 0, 2)


    int request1[] = {1, 0, 2};
    printf("Processing request from P1 for (1, 0, 2):\n");
    if (requestResources(1, request1, avail, alloc, max, need)) {
        printf("The Allocation Matrix after P1's request is:\n");
        printMatrix(alloc);
        printf("The Need Matrix after P1's request is:\n");
        printMatrix(need);
        printf("The Available Vector after P1's request is:\n");
        for (int i = 0; i < M; i++) {
            printf("%d ", avail[i]);
        }
        printf("\n");
    }

    // Process request from P4 for (3, 3, 0)
    int request4[] = {3, 3, 0};
    printf("Processing request from P4 for (3, 3, 0):\n");
    if (requestResources(4, request4, avail, alloc, max, need)) {
        printf("The Allocation Matrix after P4's request is:\n");
        printMatrix(alloc);
        printf("The Need Matrix after P4's request is:\n");
        printMatrix(need);
        printf("The Available Vector after P4's request is:\n");
        for (int i = 0; i < M; i++) {
            printf("%d ", avail[i]);
        }
        printf("\n");
    } else {
        printf("P4's request cannot be granted.\n");
    }
    // Process request from P0 for (0, 2, 0)
    int request0[] = {0, 2, 0};
    printf("Processing request from P0 for (0, 2, 0):\n");
    if (requestResources(0, request0, avail, alloc, max, need)) {
        printf("The Allocation Matrix after P0's request is:\n");
        printMatrix(alloc);
        printf("The Need Matrix after P0's request is:\n");
        printMatrix(need);
        printf("The Available Vector after P0's request is:\n");
        for (int i = 0; i < M; i++) {
            printf("%d ", avail[i]);
        }
        printf("\n");
    } else {
        printf("P0's request cannot be granted.\n");
    }

    return 0;
}


/*

    Enter the Max Matrix for each process: 
    Process 0 : 7 5 3
    Process 1 : 3 2 2
    Process 2 : 9 0 2
    Process 3 : 2 2 2
    Process 4 : 4 3 3
    Enter the Allocation Matrix for each process: 
    Process 0 : 0 1 0
    Process 1 : 2 0 0
    Process 2 : 3 0 2
    Process 3 : 2 1 1
    Process 4 : 0 0 2
    The Need Matrix is:
    7 4 3 
    1 2 2 
    6 0 0 
    0 1 1 
    4 3 1 
    System is  in a safe state.
    Processing request from P1 for (1, 0, 2):
    Request can be granted.
    The Allocation Matrix after P1's request is:
    0 1 0 
    3 0 2 
    3 0 2 
    2 1 1 
    0 0 2 
    The Need Matrix after P1's request is:
    7 4 3 
    0 2 0 
    6 0 0 
    0 1 1 
    4 3 1 
    The Available Vector after P1's request is:
    9 5 5 
    Processing request from P4 for (3, 3, 0):
    Request can be granted.
    The Allocation Matrix after P4's request is:
    0 1 0 
    3 0 2 
    3 0 2 
    2 1 1 
    3 3 2 
    The Need Matrix after P4's request is:
    7 4 3 
    0 2 0 
    6 0 0 
    0 1 1 
    1 0 1 
    The Available Vector after P4's request is:
    6 2 5 
    Processing request from P0 for (0, 2, 0):
    Request can be granted.
    The Allocation Matrix after P0's request is:
    0 3 0 
    3 0 2 
    3 0 2 
    2 1 1 
    3 3 2 
    The Need Matrix after P0's request is:
    7 2 3 
    0 2 0 
    6 0 0 
    0 1 1 
    1 0 1 
    The Available Vector after P0's request is:
    6 0 5 


*/