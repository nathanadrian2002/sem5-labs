#include<stdio.h>
#include <stdlib.h>


void main()
{
	int queue[100],n,head,i,j,k,seek=0,diff;
	float avg;
	// clrscr();
	printf("*** FCFS Disk Scheduling Algorithm ***\n");
	printf("Enter the size of Queue\t");
	scanf("%d",&n);
	printf("Enter the Queue\t");
	
	for(i=1; i<=n; i++)
	{
		scanf("%d",&queue[i]);
	}

	printf("Enter the initial head position\t");
	scanf("%d",&head);
	queue[0]=head;
	printf("\n");
	
	for(j=0;j<=n-1;j++)
	{
		diff=abs(queue[j+1]-queue[j]);
		seek+=diff;
		printf("Move from %d to %d with Seek%d\n",queue[j],queue[j+1],diff);
	}
	
	printf("\nTotal Seek Time is %d\t",seek);
	avg=seek/(float)n;
	
	printf("\nAverage Seek Time is %f\t",avg);
	// getch();
}

// Consider the following snapshot of the system. Write C program to implement
// Banker’s algorithm for deadlock avoidance. The program has to accept all inputs
// from the user. Assume the total number of instances of A,B and C are 10,5 and 7
// respectively.

// (a)
// What is the content of the matrix Need?
// (b)
// Is the system in a safe state?
// (c)
// If a request from process P1 arrives for (1, 0, 2), can the request be
// granted immediately? Display the updated Allocation, Need and Available
// matrices.
// (d)
// If a request from process P4 arrives for (3, 3, 0), can the request be
// granted immediately?
// (e)
// If a request from process P0 arrives for (0, 2, 0), can the request be
// granted immediately?