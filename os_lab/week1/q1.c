#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include<stdio.h>
#include<string.h>

// int match(char* base, int l, int r, char* pat)
// {
// 	int i = 0;
	
// 	while(pat[i] != '\0')
// 	{
// 		if (l+i == r)
// 			return 0;

// 		if (pat[i] != base[l+i])
// 			return 0;

// 		i++;
// 	}

// 	return 1;
// }

int is_in_line(char* base, int l, int r, char* pat)
{
	int pat_l = 0;
	int match = 0;
	
	while(pat[pat_l++] != '\0');

	if (pat_l > r-l)
		return 0;

	for (int i = l; i < r - pat_l + 1; i++)
	{
		match = 1;
		for(int j = 0; j < pat_l; j++)
		{
			if (pat[j] != base[l+j])
			{
				match =0;
				break;
			}	

		}

		if (match) return 1;
	}
	return 0;

}

void print_line(char* base, int s)
{
	int i = s;
	while(base[i] != '\n' || base[i] != '\0')
	{
		printf("%c", base[i]);
		i++;
	}
	printf("\n");
}


int main(int argc, char *argv[])
{
	char buffer[128];
	int nread, i = 0;

	int start = 0;
	int line_start = 0;



	printf("%d\n", argc);

	if (argc == 1)
		return -1;

	printf("%s\n", argv[1]);

	
	// Reading
	nread = read(0, buffer, 128);
	if (nread == -1)
		write(2, "A read error has occurred\n", 26);
	
	
	while(i < 128)
	{	
		if (buffer[i] == '\n')
		{
			print_line(buffer, line_start);
			line_start = i + 1;
			
		}
		if (buffer[i] == '\0')
			break;
		i++;
		
	}

	

	// printf("%d\n", match("Woww ",1, 4, argv[1])); 

	exit(0);
}