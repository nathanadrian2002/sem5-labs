#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main(int argc,char* argv[]){
	char ch[128],c;
	int in,out;
	char* ret;
	in=open("file1.in",O_RDONLY);
	if(in==-1){
		printf("File not obtained\n");
		exit(0);
	}
	int i=0,k=0;
	while(read(in,&c,1)>0){
		if(c!='\n'){
			ch[i]=c;
			i++;
		}
		else{
			k++;
			ch[i]='\0';
			i=0;
			if(strstr(ch,argv[1])==NULL){
				continue;
			}
			printf("Line %d: %s\n",k,ch);
		}
	}
	close(in);
	return 0;
}