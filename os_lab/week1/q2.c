
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	char buffer[200], c;
	int b = 0;
	int file;

	int line_count = 0;

	file = open("a.txt", O_RDONLY);
	
	while(read(file,&c,1) == 1)
	{
		if (c == '\n')
			line_count++;

		buffer[b++] = c;

		if (line_count == 20)
		{
			
			read(0, &c, 1);

			buffer[b] = '\0';
			write(2, &buffer, 200);
			b = 0;
			line_count = 0;
		}
	}

	exit(0);
}