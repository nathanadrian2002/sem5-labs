#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
void copy_character(int in,int out){
	char c;
	while(read(in,&c,1)>0){
		write(out,&c,1);
	}
}
int main(int argc,char* argv[]){
	int in,out;
	if(argc!=3){
		printf("Insufficient inputs\n");
	}
	in=open(argv[1],O_RDONLY);
	if(in==-1)
		printf("File couldn't be opened\n");
	out=open(argv[2],O_RDWR,S_IRUSR|S_IRGRP);
	if(out==-1)
		printf("File couldn't be opened\n");
	copy_character(in,out);
	return 0;
}