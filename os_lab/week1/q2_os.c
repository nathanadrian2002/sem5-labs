#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int main(int argc,char* argv[]){
	int in1,in2,i=0,k=0,p=0;
	char ch[200],c;
	if(argc!=3){
		printf("Insufficient arguments\n");
		exit(1);
	}
	in1=open(argv[1],O_RDONLY);
	if(in1==-1){
		printf("File couldn't be opened\n");
		exit(1);
	}
	while(read(in1,&c,1)>0){
		if(c!='\n'){
			ch[i]=c;
			i++;
		}
		else{
			k++;
			p++;
			ch[i]='\0';
			i=0;
			if(k==3){
				fgetc(stdin);
				k=0;
			}
			printf("Line %d: %s\n",p,ch);
		}
	}
	i=0;
	k=0;
	p=0;
	in2=open(argv[2],O_RDONLY);
	if(in2==-1){
		printf("File couldn't be opened \n");
		exit(1);
	}
	while(read(in2,&c,1)>0){
		if(c!='\n'){
			ch[i]=c;
			i++;
		}
		else{
			k++;
			p++;
			ch[i]='\0';
			i=0;
			if(k==3){
				fgetc(stdin);
				k=0;
			}
			printf("Line %d: %s\n",p,ch);
		}
	}
	return 0;
}