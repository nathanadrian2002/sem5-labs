#include<stdio.h>
#include<stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

int main()
{
	int st;
	
	if (fork())
	{
		// for (int i = 0; i < 9999999999; i++);
		printf("ppid %d\n", getpid());
		sleep(99);
		printf("Parent Exiting\n");
		// exit(0);
	}
	else
	{
		printf("Cpid %d\n", getpid());
		exit(0);
	}

	return 0;
}