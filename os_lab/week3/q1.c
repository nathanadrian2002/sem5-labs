#include<stdio.h>
#include<stdlib.h>

#include <unistd.h>

#include <sys/wait.h>

int main()
{
	int st;
	
	if (fork())
	{
		printf("Parent is Wating\n");
		wait(&st);
		printf("Child Complete\n");
	}
	else
	{
		for (int i = 1; i < 4; i ++)
			printf("%d  ", i);
		printf("\n");
	}

	return 0;
}