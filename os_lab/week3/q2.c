#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
	int st;
	
	if (fork())
	{
		wait(&st);
		printf("Process is complete\n");
	}
	else
	{
		char *args[] = {NULL};
		execvp("./q1.o", {args});
	}

	return 0;
}