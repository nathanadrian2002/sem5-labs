#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define TEXT_SZ 2048

struct shared_use_st {

	int p;
	int mat[3][3];
};

void print_board(int mat[3][3])
{
	printf("|---|---|---|\n");

	for (int i = 0; i < 3; i++)
	{
		printf("|");
		for (int j = 0; j < 3; j++)
		{
			if (mat[i][j] == 1)
				printf(" X");
			else if (mat[i][j] == 2)
				printf(" O");
			else
				printf("  ");
			printf(" |");
		}
		printf("\n|---|---|---|\n");
	}
}

void clear_board(int mat[3][3])
{
	for (int i = 0; i < 3; i ++)
	{
		for (int j = 0; j < 3; j ++)
			mat[i][j] = 0;
	}
}


int has_won(int mat[3][3])
{

	int d = 0, p1 =1, p2 = 2;

	// HOrizontal
	for (int i = 0; i < 3; i++)
	{
		if (mat[i][0] != mat[i][1])
			continue;
		if (mat[i][0] != mat[i][2])
			continue;
		if (mat[i][0] == d)
			continue;
		return mat[i][0];
	}

	// Vertical 
	for (int i = 0; i < 3; i++)
	{
		if (mat[0][i] != mat[1][i])
			continue;
		if (mat[0][i] != mat[2][i])
			continue;
		if (mat[0][i] == d)
			continue;
		return mat[0][i];
	}

	if (	mat[0][0] == mat[1][1] 
		&& 	mat[0][0] == mat[2][2]
		&&  mat[0][0] != d)
		return mat[0][0];
	
	if (	mat[0][2] == mat[1][1] 
		&& 	mat[0][2] == mat[2][0]
		&&  mat[0][2] != d)
		return mat[0][2];


	return 0;
}