
#include "tictactoe.h"

int main()
{	
	int PLAYER = 1;
	int r = 1, i;
	void *shared_memory = (void *)0;
	struct shared_use_st *shared_stuff;
	int shmid;
	shmid = shmget((key_t)1235, sizeof(struct shared_use_st), 0666 | IPC_CREAT);		
	
	shared_memory = shmat(shmid, (void *)0, 0);
	
	printf("Memory attached at %X\n", (int)shared_memory);
	shared_stuff = (struct shared_use_st *)shared_memory;


	srand((unsigned int)getpid());

	shared_stuff->p = 1;
	clear_board(shared_stuff->mat);
	while(r) {
		
		if (shared_stuff->p == PLAYER) {

			print_board(shared_stuff->mat);

			if (i = has_won(shared_stuff->mat))
			{
				printf("Player %d Has WON", i);
				break;
			}	

			printf("Enter Move: ");
			scanf("%d", &i);

			shared_stuff->mat[i/3][i%3] = PLAYER + 1;

			print_board(shared_stuff->mat);

			sleep(1);
			shared_stuff->p = !PLAYER;

			if (i = has_won(shared_stuff->mat))
			{
				printf("Player %d Has WON", i);
				break;
			}	
			
		}

	}
	
	return 0;


}