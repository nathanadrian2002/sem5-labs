
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#define TEXT_SZ 2048

struct shared_use_st {

	int written_by;
	char some_text;
};


int main()
{	
	int running = 1;
	void *shared_memory = (void *)0;
	struct shared_use_st *shared_stuff;
	int shmid;
	shmid = shmget((key_t)1234, sizeof(struct shared_use_st), 0666 | IPC_CREAT);		
		
	if (shmid == -1) return 1;

	shared_memory = shmat(shmid, (void *)0, 0);
	
	if (shared_memory == (void *)-1) return 1;

	printf("Memory attached at %X\n", (int)shared_memory);
	shared_stuff = (struct shared_use_st *)shared_memory;

	if (fork())
	{
		//Child
		srand((unsigned int)getpid());

		shared_stuff->written_by = 0;
	
		while(running) {
			
			if (shared_stuff->written_by) {
				// printf("You wrote: %s", shared_stuff->some_text);
				// sleep( rand() % 4 );  make the other process wait for us ! 
				printf("C: Parent sends: %c\n", shared_stuff->some_text);
				shared_stuff->some_text += 1;
				printf("C: Updated : %c\n", shared_stuff->some_text);
				shared_stuff->written_by = 0;
				
				// if (strncmp(shared_stuff->some_text, "end", 3) == 0) {
				// running = 0;
				sleep(2);
			}

		}
	

	if (shmdt(shared_memory) == -1) {
		return 1;
	}
	
	if (shmctl(shmid, IPC_RMID, 0) == -1) {
		return 1;
	}
	
	return 0;



	}
	else
	{
		char buffer[BUFSIZ];
		//Parent
		while(running) {

			while(shared_stuff->written_by != 0) {
				sleep(1);
				// printf("waiting for client...\n");
			}
			
			printf("Enter Alphabet:");
			scanf(" %c", &shared_stuff->some_text);
			shared_stuff->written_by = 1;
			
			while(shared_stuff->written_by != 0) {
				sleep(1);
				printf("waiting for client...\n");
			}

			printf("P: Child Response: %c\n", shared_stuff->some_text);

			
			
		}
		
		if (shmdt(shared_memory) == -1) return 1;

		exit(EXIT_SUCCESS);

	}

}