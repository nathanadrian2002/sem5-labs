#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct my_msg_st {
	long int my_msg_type;
	char some_text[BUFSIZ];
};

int is_palin(int a)
{
	int temp = 0;
	int c = a;
	while (a)
	{
		temp *= 10;
		temp += a%10;
		a/= 10;
	}

	return (temp == c);
}

int main()
{
	int msgid;
	struct my_msg_st some_data;
	long int msg_to_receive = 0;
	msgid = msgget((key_t)1234, 0666 | IPC_CREAT);
	
	if (msgid == -1) return 1;
	
	while(1) 
	{
		
		if (msgrcv( msgid, (void *)&some_data, 
					BUFSIZ,
					msg_to_receive, 0) == -1) 
			return 1;
		
		if (strncmp(some_data.some_text, "end", 3) == 0) 
			break;

		printf("%s IS ", some_data.some_text);
		if (!is_palin(atoi(some_data.some_text)))
			printf("NOT ");

		printf("a palindrome\n");
		
		
	}
		if (msgctl(msgid, IPC_RMID, 0) == -1) 
			return 1;
		
	
	return 0;
}