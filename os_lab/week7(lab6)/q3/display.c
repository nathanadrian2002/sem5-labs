#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>


#define TEXT_SZ 2048

struct shared_use_st {

	int written_by;
	char some_text[300];
};


int main()
{	
	int running = 1;
	void *shared_memory = (void *)0;
	struct shared_use_st *shared_stuff;
	int shmid;
	shmid = shmget((key_t)1235, sizeof(struct shared_use_st), 0666 | IPC_CREAT);		
	
	shared_memory = shmat(shmid, (void *)0, 0);
	
	printf("Memory attached at %X\n", (int)shared_memory);
	shared_stuff = (struct shared_use_st *)shared_memory;


	srand((unsigned int)getpid());

	shared_stuff->written_by = 0;

	while(running) {
		
		if (shared_stuff->written_by) {

			printf("Display: %s\n", shared_stuff->some_text);

			// printf("C: Updated : %c\n", shared_stuff->some_text);
			shared_stuff->written_by = 0;
		}

	}
	
	return 0;


}