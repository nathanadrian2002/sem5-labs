#include <stdio.h>
#include <dirent.h>
#include<sys/stat.h>
#include <sys/types.h>

int main()
{
	DIR * dr;

	struct dirent * en;

	struct stat stats;

	dr = opendir(".");

	if (dr){

		while((en = readdir(dr)) != NULL)
		{
			lstat(en->d_name,&stats);
			printf("Name: %s Mode: %d\n", 
				en->d_name, 
				stats.st_mode & 0777);
			
		}
		closedir(dr);
	}

	return 0;
}