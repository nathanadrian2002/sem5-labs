#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

int main()
{
	DIR * dr;

	struct dirent * en;

	struct stat stats;

	int n = 0;

	dr = opendir(".");

	if (dr){

		while((en = readdir(dr)) != NULL)
		{
			lstat(en->d_name,&stats);
			
			if(S_ISDIR(stats.st_mode)) 
				continue;

			printf("Delete %s (1/0): ", en->d_name);

			scanf("%d", &n);
			if (n==0)
				continue;

			if (remove(en->d_name) == 0)
		        printf("Deleted successfully\n");
		    else
		        printf("Unable to delete the file\n");

		}
		closedir(dr);
	}

	return 0;
}